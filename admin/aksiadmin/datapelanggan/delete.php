<?php
// Mendapatkan ID yang dikirim melalui parameter GET
require_once dirname(dirname(dirname(__DIR__))) . '/config.php';
$id = $_GET['id'];

// Mengeksekusi perintah DELETE
$sql = "DELETE FROM tabel_pelanggan WHERE id = $id";

if ($con->query($sql) === TRUE) {
    echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
    echo "<script>
            window.onload = function() {
                swal({
                    title: 'Success!',
                    text: 'Data successfully deleted!',
                    icon: 'success',
                    button: 'OK',
                }).then(function() {
                    window.location.href = '../../datapelanggan.php';
                });
            };
          </script>";
} else {
    echo "Terjadi kesalahan saat menghapus data: " . $con->error;
}

// Menutup koneksi
$con->close();
?>
