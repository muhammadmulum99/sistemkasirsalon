<?php
// ... kode untuk koneksi ke database ...
require_once dirname(dirname(dirname(__DIR__))) . '/config.php';

// Import FPDF library
require_once 'fpdf186/fpdf.php';

if (isset($_POST['tanggal_awal']) && isset($_POST['tanggal_akhir'])) {
    // ... Kode pengolahan data seperti yang telah diberikan sebelumnya ...
    $tanggalAwal = $_POST['tanggal_awal'];
    $tanggalAkhir = $_POST['tanggal_akhir'];

    // Lakukan pengolahan data di sini

    $sqltransaksihariini="SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
    $totalHariIni = $rowhari['total_hari_ini'];

    $sqltransaksipengeluaran="SELECT SUM(jumlah) AS total FROM tabel_pengeluaran WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $resulpengeluaran = mysqli_query($con, $sqltransaksipengeluaran);
    $rowpengeluaran = mysqli_fetch_assoc($resulpengeluaran);
        $totalpengeluaran = $rowpengeluaran['total'];

        $laba=$rowhari['total_hari_ini']-$rowpengeluaran['total'];
    

    $query = "SELECT * FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result = mysqli_query($con, $query);
    $transaksi = array();

    $query2 = "SELECT * FROM tabel_pengeluaran WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result2 = mysqli_query($con, $query2);
    $transaksi2 = array();
    $formattedNumbers = [];
    $formattedNumbers2 = [];
    $formattedNumber2 = number_format($totalHariIni, 0, '.', ',');
            
    // Menyimpan hasil formatted number ke dalam array
    $formattedNumbers2[] = $formattedNumber2;

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }

    if ($result2) {
        while ($row2 = mysqli_fetch_assoc($result2)) {
            $transaksi2[] = $row2;
        }
    }
    // Fungsi untuk membuat file PDF dan mengisi kontennya

        $pdf = new FPDF(); // Buat objek PDF

    // Fungsi untuk membuat footer
    function Footer() {
        global $pdf;
        // Tampilkan tanggal hari ini di posisi kanan bawah
        $pdf->SetY(-15); // Atur posisi ke 15 mm dari bawah halaman
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, 10, date('j F Y') . ', Viny Salon', 0, 0, 'R');

        // Tanda tangan
        $pdf->SetY(-30); // Atur posisi ke 30 mm dari bawah halaman
        $pdf->SetFont('Arial', 'I', 8);
        $pdf->Cell(0, 10, 'Tanda Tangan', 0, 0, 'R');
    }

        // Buat halaman baru
        $pdf->AddPage();
        

        $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(0, 10, 'Laporan Pendapatan', 0, 1, 'C');
    $pdf->SetFont('Arial', 'B', 14);
    $pdf->Cell(0, 10, 'Viny Salon', 0, 1, 'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(0, 5, 'Dari Tanggal '.$tanggalAwal.' s/d '.$tanggalAkhir, 0, 1, 'C');
    $pdf->Cell(0, 10, 'Jl.Beringin 2 (Belakang Kantor POS),Telp 0852-7035-6101 Lahewa Nias Utara', 0, 1, 'C');

    $pdf->SetLineWidth(0.5); // Mengatur ketebalan garis
    $pdf->Line(10, $pdf->GetY(), 200, $pdf->GetY()); 
    // $pdf->Image('http://localhost/skripsisemarang/admin/aksiadmin/datalaporan/logos.png', 40, 10, $logoWidth, $logoHeight);
    $pdf->Ln(10); // Atur jarak antara header


        // Tambahkan tabel data transaksi
        $pdf->SetFont('Arial', 'B', 11,false,'C');
        $pdf->Cell(10, 10, 'No', 1);
        $pdf->Cell(40, 10, 'Kode Transaksi', 1,false,'C');
        $pdf->Cell(40, 10, 'Nama Pelanggan', 1,false,'C');
        $pdf->Cell(35, 10, 'TGL Transaksi', 1,false,'C');
        $pdf->Cell(35, 10, 'Jenis Layanan', 1,false,'C');
        $pdf->Cell(30, 10, 'Tarif', 1,false,'C');
        $pdf->Ln();
        
        

        $counter = 1;
        foreach ($transaksi as $row) {
            $number = $row['tarif'];
        
            // Format angka menggunakan metode toLocaleString()
            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
            $formattedNumber = number_format($number, 0, '.', ',');
            
            // Menyimpan hasil formatted number ke dalam array
            $formattedNumbers[] = $formattedNumber;
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(10, 10, $counter, 1);
            $pdf->Cell(40, 10,'TRX-'.$row['tanggal_transaksi'].$row['id'], 1,false,'C');
            $pdf->Cell(40, 10, $row['nama_pelanggan'], 1,false,'C');
            $pdf->Cell(35, 10, date("d-m-Y", strtotime($row["tanggal_transaksi"])), 1,false,'C');
            $pdf->Cell(35, 10, $row['jenis_layanan'], 1,false,'C');
            $pdf->Cell(30, 10, $formattedNumber, 1,false,'R');
            $pdf->Ln();
            $counter++;
        }
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(160, 10, 'Total:', 1,false,'R');
        $pdf->Cell(30, 10, $formattedNumber2, 1,false,'R');
        $pdf->Ln(20);

      
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, 10, date('j F Y') . '', 0, 0, 'R');
        $pdf->Ln(8);
        $pdf->Cell(0, 10, 'admin' . '', 0, 0, 'R');
        $pdf->Ln(30);
        // Tanda tangan
       // Atur posisi ke 30 mm dari bawah halaman
        $pdf->SetFont('Arial', 'I', 8);
        $pdf->Cell(0, 10, '(........................)', 0, 0, 'R');

        // Footer();
        // Output file PDF
        $pdf->Output('laporan.pdf', 'I');
   

    mysqli_close($con);
} else {
    echo "<p>Silakan isi tanggal awal dan tanggal akhir.</p>";
}
?>
