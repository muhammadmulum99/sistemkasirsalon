<?php
// ... kode untuk koneksi ke database ...
require_once dirname(dirname(dirname(__DIR__))) . '/config.php';
if (isset($_POST['tanggal_awal']) && isset($_POST['tanggal_akhir'])) {
    $tanggalAwal = $_POST['tanggal_awal'];
    $tanggalAkhir = $_POST['tanggal_akhir'];

    // Lakukan pengolahan data di sini

    $sqltransaksihariini="SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
    $totalHariIni = $rowhari['total_hari_ini'];

    

    $query = "SELECT * FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result = mysqli_query($con, $query);
    $transaksi = array();

    $formattedNumbers = [];
    $formattedNumbers2 = [];
    $formattedNumber2 = number_format($totalHariIni, 0, '.', ',');
            
    // Menyimpan hasil formatted number ke dalam array
    $formattedNumbers2[] = $formattedNumber2;

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }

    if (!empty($transaksi)) {
        echo "

        <header style='
        background-size: cover;
        background-position: center;
        height: 300px;
        color: #ffffff; 
        padding-top: 100px;
    '>
        <h1 style='
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 20px;
            color: black;
            font-size: 24px;
        '>
            <img src='./aksiadmin/datalaporan/logos.png' alt='' style='height: 150px; width: 150px; margin-right: 10px;'>
            <div>
                <span style='text-align: center; display: block;'>Laporan Pendapatan</span>
                <span style='text-align: center; display: block;'>Salon Viny</span>
                <span style='text-align: center; display: block;'>Lahewa Nias Utara</span>
            </div>
        </h1>
    </header>
    
    ";
        echo "<table class='table table-bordered' id='laporan-table'>
                <thead>
                    <tr>
                        <th>ID Transaksi</th>
                        <th>Tanggal</th>
                        <th>Jenis Layanan</th>
                        <th>Nama Customer</th>
                        <th>Harga</th>
                    </tr>
                 
                </thead>
                <tbody>";

        foreach ($transaksi as $row) {
            $no=$row['tarif'];
            $formattedNumber = number_format($no, 0, '.', ',');
            
            // Menyimpan hasil formatted number ke dalam array
            $formattedNumbers[] = $formattedNumber;
            echo "<tr>
                    <td>".$row['id']."</td>
                    <td>".date("d-m-Y", strtotime($row["tanggal_transaksi"]))."</td>
                    <td>".$row['jenis_layanan']."</td>
                    <td>".$row['nama_pelanggan']."</td>
                    <td>". $formattedNumber."</td>
                </tr>";
        }
    
        echo "</tbody>
        <tfoot>
        <tr>
            <td colspan='3' style='text-align: right;'>Pendapatan:</td>
            <td>".$formattedNumber2."</td>
        </tr>
        
    </tfoot>
            </table>";
            echo "<form action='./aksiadmin/datalaporan/generate_pdfpendapatan.php' method='post'>
   
            <input type='date' value='$tanggalAwal' name='tanggal_awal' required hidden>
        
            
            <input type='date' value='$tanggalAkhir' name='tanggal_akhir' required hidden>
        
            <button class='btn btn-primary' type='submit'>Cetak PDF</button>
        </form>";
    } else {
        echo "Tidak ada transaksi dalam rentang tanggal yang diberikan.";
    }
} else {
    echo "Silakan isi tanggal awal dan tanggal akhir.";
}
?>
