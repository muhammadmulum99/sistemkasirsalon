<?php
// ... kode untuk koneksi ke database ...
require_once dirname(dirname(dirname(__DIR__))) . '/config.php';

// Import FPDF library
require_once 'fpdf186/fpdf.php';

if (isset($_POST['tanggal_awal']) && isset($_POST['tanggal_akhir'])) {
    // ... Kode pengolahan data seperti yang telah diberikan sebelumnya ...
    $tanggalAwal = $_POST['tanggal_awal'];
    $tanggalAkhir = $_POST['tanggal_akhir'];

    // Lakukan pengolahan data di sini

    $sqltransaksihariini="SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
    $totalHariIni = $rowhari['total_hari_ini'];

    $sqltransaksipengeluaran="SELECT SUM(jumlah) AS total FROM tabel_pengeluaran WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $resulpengeluaran = mysqli_query($con, $sqltransaksipengeluaran);
    $rowpengeluaran = mysqli_fetch_assoc($resulpengeluaran);
        $totalpengeluaran = $rowpengeluaran['total'];

        $laba=$rowhari['total_hari_ini']-$rowpengeluaran['total'];
    

    $query = "SELECT * FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result = mysqli_query($con, $query);
    $transaksi = array();

    $query2 = "SELECT * FROM tabel_pengeluaran WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result2 = mysqli_query($con, $query2);
    $transaksi2 = array();

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }

    if ($result2) {
        while ($row2 = mysqli_fetch_assoc($result2)) {
            $transaksi2[] = $row2;
        }
    }
    // Fungsi untuk membuat file PDF dan mengisi kontennya

        $pdf = new FPDF(); // Buat objek PDF

        // Fungsi untuk membuat header
    // function Header() {

    //     global $pdf;

    //     // Logo atau gambar lainnya yang ingin ditampilkan pada header
    //     $pdf->SetFont('Arial', 'B', 14);
    //     $pdf->Cell(0, 10, 'Laporan Laba Rugi Salon Viny', 0, 1, 'C');
    //     $pdf->Cell(0, 10, 'Alamat Salon Viny', 0, 1, 'C');

    //     // Tambahkan gambar/logo pada header
    //     $logoWidth = 50; // Lebar gambar/logo dalam milimeter
    //     $logoHeight = 30; // Tinggi gambar/logo dalam milimeter
    //     $pdf->Image('path/ke/direktori/gambar/logo.png', 10, 10, $logoWidth, $logoHeight);

    //     $pdf->Ln(40); // Atur jarak antara header dengan konten setelahnya
    //     // ... Kode sebelumnya ...
    // }

    // Fungsi untuk membuat footer
    function Footer() {
        global $pdf;
        // Tampilkan tanggal hari ini di posisi kanan bawah
        $pdf->SetY(-15); // Atur posisi ke 15 mm dari bawah halaman
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, 10, 'Tanggal: ' . date('Y-m-d'), 0, 0, 'R');
    }

        // Buat halaman baru
        $pdf->AddPage();

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'Laporan Pendapatan', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 10, 'Viny Salon', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, 5, 'Dari Tanggal '.$tanggalAwal.' s/d '.$tanggalAkhir, 0, 1, 'C');
        $pdf->Cell(0, 10, 'Jl.Beringin 2 (Belakang Kantor POS),Telp 0852-7035-6101 Lahewa Nias Utara', 0, 1, 'C');
    
        $pdf->SetLineWidth(0.5); // Mengatur ketebalan garis
        $pdf->Line(10, $pdf->GetY(), 200, $pdf->GetY()); 
    $pdf->Ln(10); // Atur jarak antara header

          // Tambahkan tabel data transaksi
          $pdf->SetFont('Arial', 'B', 12);
          $pdf->Cell(20, 10, 'No', 1);
          $pdf->Cell(60, 10, 'Tanggal Transaksi', 1,false,'C');
          $pdf->Cell(50, 10, 'Nama Pengeluaran', 1,false,'C');
          $pdf->Cell(60, 10, 'Jumlah Pengeluaran', 1,false,'C');
        
          $pdf->Ln();


          $counter = 1;
          foreach ($transaksi2 as $row) {
            $number = $row['jumlah'];
        
            // Format angka menggunakan metode toLocaleString()
            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
            $formattedNumber = number_format($number, 0, '.', ',');
            $pdf->SetFont('Arial', '', 12);
              $pdf->Cell(20, 10, $counter, 1);
              $pdf->Cell(60, 10, date("d-m-Y", strtotime($row["tanggal_transaksi"])), 1,false,'C');
              $pdf->Cell(50, 10, $row['nama_pengeluaran'], 1,false,'C');
              $pdf->Cell(60, 10, $formattedNumber, 1,false,'R');
              
              $pdf->Ln();
              $counter++;
          }
          $number = $rowpengeluaran['total'];
        
          // Format angka menggunakan metode toLocaleString()
          // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
          $formattedNumbert = number_format($number, 0, '.', ',');
  
          $pdf->SetFont('Arial', 'B', 14);
          $pdf->Cell(160, 10, 'Total:', 1,false,'R');
          $pdf->Cell(30, 10,'Rp.'.$formattedNumbert, 1,false,'R');
          $pdf->Ln(20);
        // $pdf->Cell(60, 10, 'Total Pengeluaran', 1);
        // $pdf->Cell(60, 10, $rowpengeluaran['total'], 1);
        // $pdf->Cell(60, 10, 'Laba', 1);
        $pdf->Ln();


        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, 10, date('j F Y') . '', 0, 0, 'R');
        $pdf->Ln(8);
        $pdf->Cell(0, 10, 'admin' . '', 0, 0, 'R');
        $pdf->Ln(30);
        // Tanda tangan
       // Atur posisi ke 30 mm dari bawah halaman
        $pdf->SetFont('Arial', 'I', 8);
        $pdf->Cell(0, 10, '(........................)', 0, 0, 'R');

        // Footer();
        // Output file PDF
        $pdf->Output('laporan.pdf', 'I');
   

    mysqli_close($con);
} else {
    echo "<p>Silakan isi tanggal awal dan tanggal akhir.</p>";
}
?>
