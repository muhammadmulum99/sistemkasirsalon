<?php
// ... kode untuk koneksi ke database ...
require_once dirname(dirname(dirname(__DIR__))) . '/config.php';





if (isset($_POST['tanggal_awal']) && isset($_POST['tanggal_akhir'])) {
    $tanggalAwal = $_POST['tanggal_awal'];
    $tanggalAkhir = $_POST['tanggal_akhir'];

    // Lakukan pengolahan data di sini

    $sqltransaksihariini="SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
    $totalHariIni = $rowhari['total_hari_ini'];

    $sqltransaksipengeluaran="SELECT SUM(jumlah) AS total FROM tabel_pengeluaran WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $resulpengeluaran = mysqli_query($con, $sqltransaksipengeluaran);
    $rowpengeluaran = mysqli_fetch_assoc($resulpengeluaran);
        $totalpengeluaran = $rowpengeluaran['total'];

        $laba=$rowhari['total_hari_ini']-$rowpengeluaran['total'];

        $formattedNumberlaba = number_format($laba, 0, '.', ',');

        $formattedNumberpengeluaran = number_format($totalpengeluaran, 0, '.', ',');
            
        // Menyimpan hasil formatted number ke dalam array
        $formattedNumberslaba[] = $formattedNumberlaba;
        $formattedNumberspengeluaran[] = $formattedNumberpengeluaran;
    

    $query = "SELECT * FROM transaksi WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result = mysqli_query($con, $query);
    $transaksi = array();

    $query2 = "SELECT * FROM tabel_pengeluaran WHERE tanggal_transaksi BETWEEN '$tanggalAwal' AND '$tanggalAkhir'";
    $result2 = mysqli_query($con, $query2);
    $transaksi2 = array();

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }

    if ($result2) {
        while ($row2 = mysqli_fetch_assoc($result2)) {
            $transaksi2[] = $row2;
        }
    }

    // cetakPDFf();
    // header('Content-Disposition: attachment; filename="laporan.pdf"');
    


    if (!empty($transaksi)) {
        $har=$rowhari['total_hari_ini'];
        $formattedNumberhar = number_format($har, 0, '.', ',');
            
        // Menyimpan hasil formatted number ke dalam array
        $formattedNumbershar[] = $formattedNumberhar;

        echo "
       
        <header style='
        background-size: cover;
        background-position: center;
        height: 300px;
        color: #ffffff; 
        padding-top: 100px;
    '>
        <h1 style='
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 20px;
            color: black;
            font-size: 24px;
        '>
            <img src='./aksiadmin/datalaporan/logos.png' alt='' style='height: 150px; width: 150px; margin-right: 10px;'>
            <div>
                <span style='text-align: center; display: block;'>Laporan Laba Rugi</span>
                <span style='text-align: center; display: block;'>Salon Viny</span>
                <span style='text-align: center; display: block;'>Lahewa Nias Utara</span>
            </div>
        </h1>
    </header>
    ";
    echo "<h2>Transaksi Data</h2>";
    echo "
    <table class='table table-bordered'>
    <thead class='thead-dark'>
        <tr>
            <th scope='col'>Pendapatan</th>
            <th scope='col'>Rp</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Pendapatan</td>
            <td>".$formattedNumberhar."</td>
        </tr>
       
        <tr class='table-info'>
            <td>Total Pendapatan</td>
            <td>".$formattedNumberhar."</td>
        </tr>
    </tbody>
    <thead class='thead-dark'>
        <tr>
            <th scope='col'>Beban</th>
            <th scope='col'>Rp</th>
        </tr>
    </thead>
    <tbody>";

    foreach ($transaksi2 as $row) {
        $tot=$row['jumlah'];
        $formattedNumberpeng = number_format($tot, 0, '.', ',');
            
        // Menyimpan hasil formatted number ke dalam array
        $formattedNumberspeng[] = $formattedNumberpeng;
        echo "<tr>
      
                <td>".$row['nama_pengeluaran']."</td> <!-- Replace with relevant column -->
                <td>".$formattedNumberpeng."</td> <!-- Replace with relevant column -->
            </tr>";
            
    }
    echo"
       
        <tr class='table-info'>
            <td>Total Beban</td>
            <td>".$formattedNumberpengeluaran."</td>
        </tr>
    </tbody>
    <tfoot class='bg-success text-white'>
        <tr>
            <th scope='row'>Laba/(Rugi) Kotor</th>
            <td> ".$formattedNumberlaba."</td>
        </tr>
    </tfoot>
</table>
    ";

      

      

 

   

    
    echo "<form action='./aksiadmin/datalaporan/generate_pdf.php' method='post'>
   
    <input type='date' value='$tanggalAwal' name='tanggal_awal' required hidden>

    
    <input type='date' value='$tanggalAkhir' name='tanggal_akhir' required hidden>

    <button class='btn btn-primary' type='submit'>Cetak PDF</button>
</form>";
} else {
    echo "Tidak ada transaksi dalam rentang tanggal yang diberikan.";
}

    mysqli_close($con);
} else {
    echo "<p>Silakan isi tanggal awal dan tanggal akhir.</p>";
}
?>


