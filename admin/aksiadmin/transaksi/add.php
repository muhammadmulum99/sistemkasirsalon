<?php
// Mendapatkan data dari form edit

require_once dirname(dirname(dirname(__DIR__))) . '/config.php';
$nama = $_POST['nama_pelanggan'];
$tanggal_transaksi = $_POST['tanggal_masuk'];
$jenis_layanan = $_POST['layanan'];
$tarif = $_POST['tarif'];
$tarifmulti = $_POST['tarifmulti'];
$dibayar = $_POST['dibayar'];
$kembalian = $_POST['kembalian'];
$nama_karyawan = $_POST['nama_karyawan'];

$jenis_layanan_string = implode(",", $jenis_layanan);
$tarif_string = implode(",", $tarifmulti);
$jenis_layanan_json = json_encode($jenis_layanan);

echo $jenis_layanan_string;
echo $tarif_string;
// die();

$data = array (
    'nama_pelanggan' => $nama,
    'tanggal_transaksi' => $tanggal_transaksi,
    'jenis_layanan' => $jenis_layanan_string,
    'tarif' => $tarif,
    'dibayar' => $dibayar,
    'kembalian' => $kembalian,
    'nama_karyawan' => $nama_karyawan
);

// print_r($data);

$sql = "INSERT INTO transaksi (nama_pelanggan, tanggal_transaksi,jenis_layanan,tarif,dibayar,kembalian,nama_karyawan) VALUES ('$nama', '$tanggal_transaksi', '$jenis_layanan_string','$tarif','$dibayar','$kembalian','$nama_karyawan');";

if ($con->query($sql) === TRUE) {
    echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
    echo "<script>
            window.onload = function() {
                swal({
                    title: 'Success!',
                    text: 'Data successfully Added!',
                    icon: 'success',
                    button: 'OK',
                }).then(function() {
                    window.location.href = '../../adminTransaksi.php';
                });
            };
          </script>";
} else {
    echo "Terjadi kesalahan saat memperbarui data: " . $con->error;
}

// Menutup koneksi
$con->close();
?>
