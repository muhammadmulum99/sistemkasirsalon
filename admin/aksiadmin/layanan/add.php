<?php
// Mendapatkan data dari form edit
require_once dirname(dirname(dirname(__DIR__))) . '/config.php';
$nama = $_POST['nama_jasa'];
$harga = $_POST['harga'];

$sql = "INSERT INTO tabel_harga (nama_jasa, harga) VALUES ('$nama', '$harga')";

if ($con->query($sql) === TRUE) {
    echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
    echo "<script>
            window.onload = function() {
                swal({
                    title: 'Success!',
                    text: 'Data successfully Added!',
                    icon: 'success',
                    button: 'OK',
                }).then(function() {
                    window.location.href = '../../pelayanan.php';
                });
            };
          </script>";
} else {
    echo "Terjadi kesalahan saat memperbarui data: " . $con->error;
}

// Menutup koneksi
$con->close();
?>
