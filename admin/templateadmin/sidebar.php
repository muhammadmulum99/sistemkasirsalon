<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="../../../skripsisemarang/admin/index.php">
    <div class="sidebar-brand-icon rotate-n-15">
     
    </div>
    <div class="sidebar-brand-text mx-3">Viny Salon<sup></sup></div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item active">
    <a class="nav-link" href="../../../skripsisemarang/admin/index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<li class="nav-item active">
    <a class="nav-link" href="../../../skripsisemarang/admin/datapelanggan.php">
    <i class="fas fa-solid fa-user-tag"></i>
        <span>Data Customer</span></a>
</li>


<li class="nav-item active">
    <a class="nav-link" href="../../../skripsisemarang/admin/datapengeluaran.php">
    <i class="fas fa-file-invoice-dollar"></i>
        <span>Pengeluaran</span></a>
</li>
<!-- Divider -->
<!-- Nav Item - Charts -->
<li class="nav-item sidebar-toggled">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2"
        aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-solid fa-envelope-open-text"></i>
        <span>Master Laporan</span>
    </a>
    <div id="collapsePages2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
           
            <a class="collapse-item" href="../../../skripsisemarang/admin/laporanlayanan.php">Laporan Pendapatan</a>
            <a class="collapse-item" href="../../../skripsisemarang/admin/laporanpengeluaran.php">Laporan Pengeluaran</a>
            <a class="collapse-item" href="../../../skripsisemarang/admin/laporanlaba.php">Laporan Laba Rugi</a>
         
        </div>
    </div>
</li>

<!-- Heading -->


<!-- Nav Item - Pages Collapse Menu -->


<!-- Nav Item - Utilities Collapse Menu -->


<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Master data
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
        aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Master data</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
           
            <a class="collapse-item" href="../../../skripsisemarang/admin/datakaryawan.php">Data Karyawan</a>
            <a class="collapse-item" href="../../../skripsisemarang/admin/pelayanan.php">Daftar Harga Layanan</a>
          
        </div>
    </div>
</li>










<!-- Nav Item - Tables -->
<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<li class="nav-item active">
    <a class="nav-link" href="../admin/adminTransaksi.php">
    <i class="fas fa-solid fa-dollar-sign"></i>
        <span>Transaksi Pelanggan</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

<!-- Sidebar Message -->


</ul>