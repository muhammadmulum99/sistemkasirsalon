<?php
include '../config.php';

// mengaktifkan session
session_start();

// cek apakah user telah login, jika belum login maka di alihkan ke halaman login
if ($_SESSION['status'] != "login") {
	header("location:../index.php");
}

// menampilkan pesan selamat datang
// echo "Hai, selamat datang " . $_SESSION['username'];

?>


<?php include("./templateadmin/header.php"); ?>


<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<?php include "./templateadmin/sidebar.php" ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<?php include './templateadmin/topbar.php'; ?>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<?php include "./content/laporan.php"; ?>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->

			<?php include("./templateadmin/footer.php"); ?>