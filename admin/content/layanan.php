<?php
require_once dirname(dirname(__DIR__)) . '/config.php';


$sql = "SELECT * FROM tabel_harga order by nama_jasa asc";
$result = $con->query($sql);
$formattedNumbers = [];

?>


<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 text-center">Halaman Layanan</h1>
    <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModal">
        Add Data Layanan
    </button>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data layanan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Layanan Jasa</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                
                    <tbody>
                        <?php
                        $no = 1;
                        while ($row = $result->fetch_assoc()) {
                            $number = $row['harga'];
        
        // Format angka menggunakan metode toLocaleString()
        // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
        $formattedNumber = number_format($number, 0, '.', ',');
        
        // Menyimpan hasil formatted number ke dalam array
        $formattedNumbers[] = $formattedNumber;
                            echo "<tr>";
                            echo "<td>". $no. "</td>";
                          
                            echo "<td>" . $row["nama_jasa"] . "</td>";
                            echo "<td>" . $formattedNumber. "</td>";
                            echo "<td>";
                            echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#editModal" . $row["id"] . "'>
                            <i class='fas fa-edit'></i>
                          </button>"; // Tombol delete dengan link ke halaman delete.php
                            echo " | ";
                            echo "<button type='button' class='btn btn-danger' onclick='confirmDelete(" . $row["id"] . ")'><i class='fas fa-trash'></i></button>"; // Tombol delete dengan SweetAlert konfirmasi
                           
                            echo "</td>";
                            echo "</tr>";
                            $no++;


                            // Modal edit untuk setiap baris data
                            // Modal edit untuk setiap baris data
                            echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                            echo "<div class='modal-dialog'>";
                            echo "<div class='modal-content'>";
                            echo "<div class='modal-header'>";
                            echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                            echo "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>";
                            echo "</div>";
                            echo "<div class='modal-body'>";
                            echo "<form action='./aksiadmin/layanan/edit.php' method='POST'>";
                            echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                            echo "<div class='mb-3'>";
                            echo "<label for='nama' class='form-label'>Nama</label>";
                            echo "<input type='text' class='form-control' id='nama_jasa' name='nama_jasa' value='" . $row["nama_jasa"] . "'>";
                            echo "</div>";
                            echo "<div class='mb-3'>";
                            echo "<label for='harga' class='form-label'>Email</label>";
                            echo "<input type='harga' class='form-control' id='harga' name='harga' value='" . $row["harga"] . "'>";
                            echo "</div>";
                            echo "</div>";
                            echo "<div class='modal-footer'>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                            echo "<button type='submit' class='btn btn-primary'>Simpan Perubahan</button>";
                            echo "</div>";
                            echo "</form>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                        }

                     



                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal Add</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action='./aksiadmin/layanan/add.php' method='POST'>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Jasa </label>
                        <input type="text" class="form-control" id="nama_jasa" name="nama_jasa" aria-describedby="emailHelp" placeholder="Enter Nama Jasa">
                       
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Harga</label>
                        <input type="number" class="form-control" id="harga" placeholder="Harga Jasa" name="harga">
                    </div>
                  
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
               
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    function confirmDelete(id) {
        Swal.fire({
            title: 'Konfirmasi Hapus Data',
            text: 'Apakah Anda yakin ingin menghapus data ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                // Redirect atau melakukan proses delete ke halaman delete.php dengan mengirimkan parameter ID
                window.location.href = './aksiadmin/layanan/delete.php?id=' + id;
            }
        });
    }


let harga= 
console.log(harga);
    
</script>

