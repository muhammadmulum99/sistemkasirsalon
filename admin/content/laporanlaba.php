<?php
// ... kode untuk koneksi ke database ...
require_once dirname(dirname(__DIR__)) . '/config.php';

$tanggalAwal = date('Y-m-d'); // Mengambil tanggal saat ini (format: YYYY-MM-DD)
$tanggalAkhir = date('Y-m-d');
    // Lakukan pengolahan data di sini
    $sqltransaksihariini="SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE tanggal_transaksi ='$tanggalAwal'";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
    $totalHariIni = $rowhari['total_hari_ini'];

    $sqltransaksipengeluaran="SELECT SUM(jumlah) AS total FROM tabel_pengeluaran WHERE tanggal_transaksi ='$tanggalAwal'";
    $resulpengeluaran = mysqli_query($con, $sqltransaksipengeluaran);
    $rowpengeluaran = mysqli_fetch_assoc($resulpengeluaran);
        $totalpengeluaran = $rowpengeluaran['total'];

        $laba=$rowhari['total_hari_ini']-$rowpengeluaran['total'];
    $query = "SELECT * FROM transaksi WHERE tanggal_transaksi= '$tanggalAwal'";
    $result = mysqli_query($con, $query);
    $transaksi = array();

    $query2 = "SELECT * FROM tabel_pengeluaran WHERE tanggal_transaksi= '$tanggalAwal'";
    $result2 = mysqli_query($con, $query2);
    $transaksi2 = array();

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }

    if ($result2) {
        while ($row2 = mysqli_fetch_assoc($result2)) {
            $transaksi2[] = $row2;
        }
    }


?>
<!DOCTYPE html>
<html>
<head>
    <title>Laporan Laba</title>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/vfs_fonts.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
    <div class="container">
        <h3 class="mb-2 text-gray-800 text-center">Laporan Laba</h3>
        <div class="form-row">
        <div class="form-group col-md-6">
        <?php
                date_default_timezone_set('Asia/Jakarta');
                $currentDate = date('Y-m-d'); ?>
            <label for="tanggal_awal">Tanggal Awal</label>
            <input type="date" class="form-control" id="tanggal_awal" required>
        </div>
        <div class="form-group col-md-6">
            <label for="tanggal_akhir">Tanggal Akhir</label>
            <input type="date" class="form-control" id="tanggal_akhir" required>
        </div>
        </div>
       
        <button type="button" class="btn btn-primary" onclick="tampilkanLaporan()">Tampilkan Laporan</button>

        <hr>

        <div id="laporan-container"></div>

        <!-- <header style='
        background-size: cover;
        background-position: center;
        height: 300px;
        color: #ffffff; 
        padding-top: 100px;
    '>
        <h1 style='
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 20px;
            color: black;
            font-size: 24px;
        '>
            <img src='./aksiadmin/datalaporan/logos.png' alt='' style='height: 150px; width: 150px; margin-right: 10px;'>
            <div>
                <span style='text-align: center; display: block;'>Laporan Laba Rugi</span>
                <span style='text-align: center; display: block;'>Salon Viny</span>
                <span style='text-align: center; display: block;'>Lahewa Nias Utara</span>
            </div>
        </h1>
    </header> -->
    <!-- <h2>Transaksi Data</h2>
    
    <table class='table table-bordered'>
    <thead class='thead-dark'>
        <tr>
            <th scope='col'>Pendapatan</th>
            <th scope='col'>Rp</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Pendapatan</td>
            <td><?php echo $rowhari['total_hari_ini']?></td>
        </tr>
       
        <tr class='table-info'>
            <td>Total Pendapatan</td>
            <td><?php echo $rowhari['total_hari_ini']?></td>
        </tr>
    </tbody>
    <thead class='thead-dark'>
        <tr>
            <th scope='col'>Beban</th>
            <th scope='col'>Rp</th>
        </tr>
    </thead>
    <tbody>
<?php
    // foreach ($transaksi2 as $row) {
    //     echo "<tr>
      
    //             <td>".$row['nama_pengeluaran']."</td> <!-- Replace with relevant column -->
    //             <td>".$row['jumlah']."</td> <!-- Replace with relevant column -->
    //         </tr>";
            
    // }

    ?>
        <tr class='table-info'>
            <td>Total Beban</td>
            <td><?php echo $rowpengeluaran['total']?></td>
        </tr>
    </tbody>
    <tfoot class='bg-success text-white'>
        <tr>
            <th scope='row'>Laba/(Rugi) Kotor</th>
            <td><?php echo $laba ?></td>
        </tr>
    </tfoot>
</table> -->
        <script>
            function tampilkanLaporan() {
                var tanggalAwal = document.getElementById("tanggal_awal").value;
                var tanggalAkhir = document.getElementById("tanggal_akhir").value;

                $.ajax({
                    type: "POST",
                    url: "./aksiadmin/datalaporan/laporanlaba.php",
                    data: {
                        tanggal_awal: tanggalAwal,
                        tanggal_akhir: tanggalAkhir
                    },
                    success: function(response) {
                        $("#laporan-container").html(response);
                        console.log(response);
                        //cetakPDF(); // Memanggil fungsi cetakPDF setelah tabel dimasukkan ke dalam kontainer
                    },
                    error: function() {
                        $("#laporan-container").html("Terjadi kesalahan dalam memproses permintaan.");
                    }
                });
            }
            function cetakPDF() {
    var laporanTable = document.getElementById("laporan-table");

    if (laporanTable) {
        var tableData = [];
        var rows = Array.from(laporanTable.getElementsByTagName("tr"));

        // Iterasi melalui setiap baris tabel
        rows.forEach(function(row) {
            var cells = Array.from(row.getElementsByTagName("td"));
            var rowCells = [];

            // Iterasi melalui setiap sel dalam baris
            cells.forEach(function(cell) {
                rowCells.push({
                    text: cell.innerText
                });
            });

            // Tambahkan baris ke data tabel
            tableData.push(rowCells);
        });

        var docDefinition = {
            content: [
                { text: "Laporan Transaksi", fontSize: 18, bold: true, margin: [0, 0, 0, 10] },
                {
                    table: {
                        body: [tableData]
                    },
                   // layout: "headerLineOnly" // Atur layout tabel menjadi "lightHorizontalLines" atau "headerLineOnly"
                }
            ],
            styles: {
                table: {
                    fontSize: 12,
                    alignment: "left"
                }
            }
        };

        pdfMake.createPdf(docDefinition).download("laporan_transaksi.pdf");
    } else {
        console.error("Tabel dengan ID 'laporan-table' tidak ditemukan.");
    }
}
        </script>
</body>
</html>
