<?php
require_once dirname(dirname(__DIR__)) . '/config.php';


$sqlpelanggan = "SELECT * FROM tabel_pelanggan";
$result = $con->query($sqlpelanggan);

$sqlkaryawan = "SELECT * FROM tabel_karyawan";
$resultkaryawan = $con->query($sqlkaryawan);


$sqltransaksi = "SELECT sum(tarif) as total FROM transaksi";
$resulttransaksi = $con->query($sqltransaksi);

$sqltransaksi2 = "SELECT sum(jumlah) as total FROM tabel_pengeluaran";
$resulttransaksi2 = $con->query($sqltransaksi2);
$rowpengeluaran = mysqli_fetch_assoc($resulttransaksi2);
$pengeluaran = $rowpengeluaran['total'];


$sqltransaksihariini = "SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE DATE(tanggal_transaksi) = CURDATE();";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
$totalHariIni = $rowhari['total_hari_ini'];
//$resulttransaksihariini = $con->query($sqltransaksihariini);

$totalpelanggan = $result->num_rows;
$totalkaryawan = $resultkaryawan->num_rows;
$totaltransaksi = $resulttransaksi->num_rows;
$totalpengeluaran = $resulttransaksi2->num_rows;


$row = mysqli_fetch_assoc($resulttransaksi);
// $rowhariini = mysqli_fetch_assoc($resulttransaksihariini);
$total = $row['total'];
// $totalhariini = $row['total_hari_ini'];
// print_r(json_encode($resulttransaksihariini));
// echo($totalHariIni)
$start_date = date('Y-m-d'); // Mengambil tanggal saat ini (format: YYYY-MM-DD)
$end_date = "2023-08-05";    // Ganti dengan tanggal end yang diinginkan

$transaksi = "SELECT * FROM transaksi WHERE tanggal_transaksi = '$start_date' ORDER BY tanggal_transaksi DESC";

$hasil = $con->query($transaksi);


$transaksi2 = "SELECT * FROM tabel_pengeluaran WHERE tanggal_transaksi = '$start_date'";
$hasil2 = $con->query($transaksi2);


$peng = "SELECT sum(jumlah) as pengeluaran FROM tabel_pengeluaran WHERE tanggal_transaksi = '$start_date'";
$hasilpenge = $con->query($peng);
$rowpenge = mysqli_fetch_assoc($hasilpenge);
$penge = $rowpenge['pengeluaran'];

$formattedNumbers = [];
$formattedNumbers2 = [];
$formattedNumbers3 = [];


?>

<!DOCTYPE html>
<html>

<head>
    <title>Laporan Transaksi</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/vfs_fonts.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <div class="container">
        <h3 class="mb-2 text-gray-800 text-center">Halaman Laporan Pengeluaran</h3>
        <div class="form-row">
            <div class="form-group col-md-6">
                <?php
                date_default_timezone_set('Asia/Jakarta');
                $currentDate = date('Y-m-d'); ?>
                <label for="tanggal_awal">Tanggal Awal</label>
                <input type="date" class="form-control" id="tanggal_awal" required>
            </div>
            <div class="form-group col-md-6">
                <label for="tanggal_akhir">Tanggal Akhir</label>
                <input type="date" class="form-control" id="tanggal_akhir" required>
            </div>
        </div>

        <button type="button" class="btn btn-primary" onclick="tampilkanLaporan()">Tampilkan Laporan</button>

        <hr>

        <div id="laporan-container"></div>


        <div class="card">

            <!-- <div class="card-body">
                <p class="font-weight-bold"> Pengeluaran Hari Ini</p>


                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>Tanggal Transaksi</th>
                                <th>Nama Pengeluaran</th>
                                <th>Jumlah Pengeluaran</th>
                                <th>Keterangan</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                                <th></th>
                             
                                <th><?php echo $penge ?></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $nomor = 1;

                            while ($row = $hasil2->fetch_assoc()) {

                                $number = $row['jumlah'];

                                // Format angka menggunakan metode toLocaleString()
                                // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
                                $formattedNumber = number_format($number, 0, '.', ',');

                                // Menyimpan hasil formatted number ke dalam array
                                $formattedNumbers[] = $formattedNumber;
                                echo "<tr>";
                                echo "<td>" . $nomor . "</td>";
                                echo "<td>" . $row["tanggal_transaksi"] . "</td>";
                                echo "<td>" . $row["nama_pengeluaran"] . "</td>";
                                echo "<td>" . $formattedNumber . "</td>";
                                echo "<td>" . $row["keterangan"] . "</td>";
                                echo "<td>";

                                echo "</td>";
                                echo "</tr>";


                                // Modal edit untuk setiap baris data
                                // Modal edit untuk setiap baris data
                                echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                                echo "<div class='modal-dialog'>";
                                echo "<div class='modal-content'>";
                                echo "<div class='modal-header'>";
                                echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                                echo "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>";
                                echo "</div>";
                                echo "<div class='modal-body'>";
                                echo "<form action='./aksiadmin/pengeluaran/edit.php' method='POST'>";
                                echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                                echo "<div class='mb-3'>";
                                echo "<label for='tanggal_transaksi' class='form-label'>Tanggal Transaksi</label>";
                                echo "<input type='date' class='form-control' id='tanggal_transaksi' name='tanggal_transaksi' value='" . $row["tanggal_transaksi"] . "'>";
                                echo "</div>";
                                echo "<div class='mb-3'>";
                                echo "<label for='no_telp' class='form-label'>Nama Pengeluaran</label>";
                                echo "<input type='text' class='form-control' id='nama_pengeluaran' name='nama_pengeluaran' value='" . $row["nama_pengeluaran"] . "'>";
                                echo "</div>";

                                echo "<div class='mb-3'>";
                                echo "<label for='jumlah' class='form-label'>Jumlah</label>";
                                echo "<input type='number' class='form-control' id='jumlah' name='jumlah' value='" . $row["jumlah"] . "'>";
                                echo "</div>";


                                echo "<div class='mb-3'>";
                                echo "<label for='keterangan' class='form-label'>Keterangan</label>";
                                echo "<input type='text' class='form-control' id='keterangan' name='keterangan' value='" . $row["keterangan"] . "'>";
                                echo "</div>";

                                echo "</div>";
                                echo "<div class='modal-footer'>";
                                echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                                echo "<button type='submit' class='btn btn-primary'>Simpan Perubahan</button>";
                                echo "</div>";
                                echo "</form>";
                                echo "</div>";
                                echo "</div>";
                                echo "</div>";


                                $nomor++;
                            }



                            ?>
                        </tbody>
                    </table>
                </div> -->

                <script>
                    function tampilkanLaporan() {
                        var tanggalAwal = document.getElementById("tanggal_awal").value;
                        var tanggalAkhir = document.getElementById("tanggal_akhir").value;

                        $.ajax({
                            type: "POST",
                            url: "./aksiadmin/datalaporan/laporanpengeluaran.php",
                            data: {
                                tanggal_awal: tanggalAwal,
                                tanggal_akhir: tanggalAkhir
                            },
                            success: function(response) {
                                $("#laporan-container").html(response);
                                console.log(response);
                                //cetakPDF(); // Memanggil fungsi cetakPDF setelah tabel dimasukkan ke dalam kontainer
                            },
                            error: function() {
                                $("#laporan-container").html("Terjadi kesalahan dalam memproses permintaan.");
                            }
                        });
                    }



                    function cetakPDF() {
                        var laporanTable = document.getElementById("laporan-table");

                        if (laporanTable) {
                            var tableData = [];
                            var rows = Array.from(laporanTable.getElementsByTagName("tr"));

                            // Iterasi melalui setiap baris tabel
                            rows.forEach(function(row) {
                                var cells = Array.from(row.getElementsByTagName("td"));
                                var rowCells = [];

                                // Iterasi melalui setiap sel dalam baris
                                cells.forEach(function(cell) {
                                    rowCells.push({
                                        text: cell.innerText
                                    });
                                });

                                // Tambahkan baris ke data tabel
                                tableData.push(rowCells);
                            });

                            var docDefinition = {
                                content: [{
                                        text: "Laporan Transaksi",
                                        fontSize: 18,
                                        bold: true,
                                        margin: [0, 0, 0, 10]
                                    },
                                    {
                                        table: {
                                            body: [tableData]
                                        },
                                        // layout: "headerLineOnly" // Atur layout tabel menjadi "lightHorizontalLines" atau "headerLineOnly"
                                    }
                                ],
                                styles: {
                                    table: {
                                        fontSize: 12,
                                        alignment: "left"
                                    }
                                }
                            };

                            pdfMake.createPdf(docDefinition).download("laporan_pengeluaran.pdf");
                        } else {
                            console.error("Tabel dengan ID 'laporan-table' tidak ditemukan.");
                        }
                    }
                </script>
            </div>
</body>

</html>