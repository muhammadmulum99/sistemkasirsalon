<?php
require_once dirname(dirname(__DIR__)) . '/config.php';


$sql = "SELECT * FROM tabel_karyawan";
$result = $con->query($sql);

?>


<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 text-center">Halaman Karyawan</h1>
    <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModal">
        Add Data Karyawan
    </button>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Karyawan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama karyawan</th>
                            <th>Alamat</th>
                            <th>No Telp</th>
                            <th>Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                        <?php
                         $nomor = 1;

                        while ($row = $result->fetch_assoc()) {
                            echo "<tr>";
                            echo "<td>" . $nomor. "</td>";
                            echo "<td>" . $row["nama_karyawan"] . "</td>";
                            echo "<td>" . $row["alamat"] . "</td>";
                            echo "<td>" . $row["no_telp"] . "</td>";
                            echo "<td>" . $row["jabatan"] . "</td>";
                            echo "<td>";
                            echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#editModal" . $row["id"] . "'>
                            <i class='fas fa-edit'></i>
                          </button>"; // Tombol delete dengan link ke halaman delete.php
                            echo " | ";
                            echo "<button type='button' class='btn btn-danger' onclick='confirmDelete(" . $row["id"] . ")'><i class='fas fa-trash'></i></button>"; // Tombol delete dengan SweetAlert konfirmasi

                            echo "</td>";
                            echo "</tr>";


                            // Modal edit untuk setiap baris data
                            // Modal edit untuk setiap baris data
                            echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                            echo "<div class='modal-dialog'>";
                            echo "<div class='modal-content'>";
                            echo "<div class='modal-header'>";
                            echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal' aria-label='Close'></button>";
                            echo "</div>";
                            echo "<div class='modal-body'>";
                            echo "<form action='./aksiadmin/datakaryawan/edit.php' method='POST'>";
                            echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                            echo "<div class='mb-3'>";
                            echo "<label for='nama_karyawan' class='form-label'>Nama Karyawan</label>";
                            echo "<input type='text' class='form-control' id='nama_karyawan' name='nama_karyawan' value='" . $row["nama_karyawan"] . "'>";
                            echo "</div>";
                            echo "<div class='mb-3'>";
                            echo "<label for='alamat' class='form-label'>Alamat</label>";
                            echo "<input type='text' class='form-control' id='alamat' name='alamat' value='" . $row["alamat"] . "'>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='no_telp' class='form-label'>No Telp</label>";
                            echo "<input type='text' class='form-control' id='no_telp' name='no_telp' value='" . $row["no_telp"] . "'>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='jabatan' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='jabatan' name='jabatan' value='" . $row["jabatan"] . "'>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='username' class='form-label'>Username</label>";
                            echo "<input type='text' class='form-control' id='username' name='username' value='" . $row["username"] . "'>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='jabatan' class='form-label'>Password</label>";
                            echo "<input type='password' class='form-control' id='password' name='password' value='" . $row["password"] . "'>";
                            echo "</div>";

                            echo "</div>";
                            echo "<div class='modal-footer'>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                            echo "<button type='submit' class='btn btn-primary'>Simpan Perubahan</button>";
                            echo "</div>";
                            echo "</form>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";



                            $nomor++;
                        }



                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal Add</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action='./aksiadmin/datakaryawan/add.php' method='POST'>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama karyawan </label>
                        <input type="text" class="form-control" id="nama_karyawan" name="nama_karyawan" aria-describedby="emailHelp" placeholder="Enter Nama Jasa">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Alamat" name="alamat">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">No Telepon</label>
                        <input type="text" class="form-control" id="no_telp" placeholder="No Telepon" name="no_telp">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputPassword1">Jabatan</label>
                        <input type="text" class="form-control" id="jabatan" placeholder="Jabatan" name="jabatan">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Username</label>
                        <input type="text" class="form-control" id="username" placeholder="Username" name="username">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                    </div>


                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    function confirmDelete(id) {
        Swal.fire({
            title: 'Konfirmasi Hapus Data',
            text: 'Apakah Anda yakin ingin menghapus data ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                // Redirect atau melakukan proses delete ke halaman delete.php dengan mengirimkan parameter ID
                window.location.href = './aksiadmin/datakaryawan/delete.php?id=' + id;
            }
        });
    }
</script>