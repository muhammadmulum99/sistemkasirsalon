<?php
require_once dirname(dirname(__DIR__)) . '/config.php';


$sqlpelanggan = "SELECT * FROM tabel_pelanggan";
$result = $con->query($sqlpelanggan);

$sqlkaryawan = "SELECT * FROM tabel_karyawan";
$resultkaryawan = $con->query($sqlkaryawan);


$sqltransaksi = "SELECT sum(tarif) as total FROM transaksi";
$resulttransaksi = $con->query($sqltransaksi);

$sqltransaksi2 = "SELECT sum(jumlah) as total FROM tabel_pengeluaran";
$resulttransaksi2 = $con->query($sqltransaksi2);
$rowpengeluaran = mysqli_fetch_assoc($resulttransaksi2);
$pengeluaran = $rowpengeluaran['total'];


$sqltransaksihariini = "SELECT SUM(tarif) AS total_hari_ini FROM transaksi WHERE DATE(tanggal_transaksi) = CURDATE();";
$resulthari = mysqli_query($con, $sqltransaksihariini);
$rowhari = mysqli_fetch_assoc($resulthari);
$totalHariIni = $rowhari['total_hari_ini'];
//$resulttransaksihariini = $con->query($sqltransaksihariini);

$totalpelanggan = $result->num_rows;
$totalkaryawan = $resultkaryawan->num_rows;
$totaltransaksi = $resulttransaksi->num_rows;
$totalpengeluaran = $resulttransaksi2->num_rows;


$row = mysqli_fetch_assoc($resulttransaksi);
// $rowhariini = mysqli_fetch_assoc($resulttransaksihariini);
$total = $row['total'];
// $totalhariini = $row['total_hari_ini'];
// print_r(json_encode($resulttransaksihariini));
// echo($totalHariIni)
$start_date = date('Y-m-d'); // Mengambil tanggal saat ini (format: YYYY-MM-DD)
$end_date = "2023-08-05";    // Ganti dengan tanggal end yang diinginkan

$transaksi = "SELECT * FROM transaksi WHERE tanggal_transaksi = '$start_date' ORDER BY tanggal_transaksi DESC";

$hasil = $con->query($transaksi);


$transaksi2 = "SELECT * FROM tabel_pengeluaran WHERE tanggal_transaksi = '$start_date'";
$hasil2 = $con->query($transaksi2);


$peng = "SELECT sum(jumlah) as pengeluaran FROM tabel_pengeluaran WHERE tanggal_transaksi = '$start_date'";
$hasilpenge = $con->query($peng);
$rowpenge = mysqli_fetch_assoc($hasilpenge);
$penge = $rowpenge['pengeluaran'];

$formattedNumbers = [];
$formattedNumbers2 = [];
$formattedNumbers3 = [];


// $number = $row['jumlah'];
        
// Format angka menggunakan metode toLocaleString()
// Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
$tot = $rowhari['total_hari_ini'];
$peng = $rowpenge['pengeluaran'];
$formattedNumberpenge = number_format($tot, 0, '.', ',');
$formattedNumbertotal = number_format($peng, 0, '.', ',');

// Menyimpan hasil formatted number ke dalam array
$formattedNumberspeng[] = $formattedNumberpenge;
$formattedNumberstot[] = $formattedNumbertotal;


?>


<div class="container-fluid">

    <!-- Page Heading -->
    <!-- <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="../admin/laporanlayanan.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div> -->

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Jumlah Pelanggan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totalpelanggan ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Jumlah Karyawan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totalkaryawan ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Transaksi
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div id="total" class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Rp.<?php echo $total ?></div>
                                </div>

                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Pengeluaran
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div id="pengeluaran" class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Rp.<?php echo $pengeluaran ?></div>
                                </div>

                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Tranksaksi Hari ini</div>
                            <div id="hariini" class="h5 mb-0 font-weight-bold text-gray-800">Rp.<?php echo $totalHariIni ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->

    <div class="row">

        <div class="col">
            <div class="card">
                <div class="card-body">
                    <p class="font-weight-bold"> Pendapatan Hari Ini</p>


                    <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Total</th>
                            <th>Nama Pelanggan</th>
                            <th>Tanggal</th>
                            <th>Jenis Pembayaran</th>
                            <th>Tarif</th>
                            <th>Dibayar</th>
                            <th>Kembalian</th>
                            <th>Nama Karyawan</th>
                            
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Total</th>
                            <th> </th>
                            <th></th>
                            <th> </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $formattedNumbertotal ?> </th>
                           
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $nomor = 1;

                        while ($row = $hasil->fetch_assoc()) {
                            $number = $row['kembalian'];
                            $number2 = $row['tarif'];
                            $number3 = $row['dibayar'];
        
                            // Format angka menggunakan metode toLocaleString()
                            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
                            $formattedNumber = number_format($number, 0, '.', ',');
                            $formattedNumber2 = number_format($number2, 0, '.', ',');
                            $formattedNumber3 = number_format($number2, 0, '.', ',');
                            
                            // Menyimpan hasil formatted number ke dalam array
                            $formattedNumbers[] = $formattedNumber;
                            $formattedNumbers2[] = $formattedNumber2;
                            $formattedNumbers3[] = $formattedNumber3;
                            echo "<tr>";
                            echo "<td>" . $nomor . "</td>";
                            echo "<td>" . $row["nama_pelanggan"] . "</td>";
                            echo "<td>" . date("d-m-Y", strtotime($row["tanggal_transaksi"])). "</td>";
                            echo "<td>" . $row["jenis_layanan"] . "</td>";
                            echo "<td>" . $formattedNumber2 . "</td>";
                            echo "<td>" . $formattedNumber3 . "</td>";
                            echo "<td>" . $formattedNumber . "</td>";
                            echo "<td>" . $row["nama_karyawan"] . "</td>";
                          
                            echo "</tr>";


                            // Modal edit untuk setiap baris data
                            // Modal edit untuk setiap baris data
                            echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                            echo "<div class='modal-dialog'>";
                            echo "<div class='modal-content'>";
                            echo "<div class='modal-header'>";
                            echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                            echo "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>";
                            echo "</div>";
                            echo "<div class='modal-body'>";
                            echo "<form action='./aksiadmin/datakaryawan/edit.php' method='POST'>";
                            echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                            echo "<div class='mb-3'>";
                            echo "<label for='nama_karyawan' class='form-label'>Nama Karyawan</label>";
                            echo "<input type='text' class='form-control' id='nama_karyawan' name='nama_pelanggan' value='" . $row["nama_pelanggan"] . "' readonly>";
                            echo "</div>";
                            echo "<div class='mb-3'>";
                            echo "<label for='alamat' class='form-label'>Alamat</label>";
                            echo "<input type='text' class='form-control' id='alamat' name='alamat' value='" . $row["tanggal_transaksi"] . "'readonly>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='no_telp' class='form-label'>No Telp</label>";
                            echo "<input type='text' class='form-control' id='no_telp' name='no_telp' value='" . $row["jenis_layanan"] . "'readonly>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='jabatan' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='jabatan' name='jabatan' value='" . $row["tarif"] . "'readonly>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='jabatan' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='jabatan' name='jabatan' value='" . $row["dibayar"] . "'readonly>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='jabatan' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='jabatan' name='jabatan' value='" . $row["kembalian"] . "'readonly>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='jabatan' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='jabatan' name='jabatan' value='" . $row["nama_karyawan"] . "'readonly>";
                            echo "</div>";

                            echo "</div>";
                            echo "<div class='modal-footer'>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                            echo "</div>";
                            echo "</form>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";



                            $nomor++;
                        }



                        ?>
                    </tbody>
                </table>
            </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">

                <div class="card-body">
                    <p class="font-weight-bold"> Pengeluaran Hari Ini</p>


                    <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Tanggal Transaksi</th>
                            <th>Nama Pengeluaran</th>
                            <th>Jumlah Pengeluaran</th>
                            <th>Keterangan</th>
                           
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                        <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                          
                            <th><?php echo $formattedNumbertotal ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $nomor = 1;

                        while ($row = $hasil2->fetch_assoc()) {

                            $number = $row['jumlah'];
        
                            // Format angka menggunakan metode toLocaleString()
                            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
                            $formattedNumber = number_format($number, 0, '.', ',');
                            
                            // Menyimpan hasil formatted number ke dalam array
                            $formattedNumbers[] = $formattedNumber;
                            echo "<tr>";
                            echo "<td>" . $nomor . "</td>";
                            echo "<td>" . date("d-m-Y", strtotime($row["tanggal_transaksi"])). "</td>";
                            echo "<td>" . $row["nama_pengeluaran"] . "</td>";
                            echo "<td>" . $formattedNumber . "</td>";
                            echo "<td>" . $row["keterangan"] . "</td>";
                            echo "<td>";
                           
                            echo "</td>";
                            echo "</tr>";


                            // Modal edit untuk setiap baris data
                            // Modal edit untuk setiap baris data
                            echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                            echo "<div class='modal-dialog'>";
                            echo "<div class='modal-content'>";
                            echo "<div class='modal-header'>";
                            echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                            echo "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>";
                            echo "</div>";
                            echo "<div class='modal-body'>";
                            echo "<form action='./aksiadmin/pengeluaran/edit.php' method='POST'>";
                            echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                            echo "<div class='mb-3'>";
                            echo "<label for='tanggal_transaksi' class='form-label'>Tanggal Transaksi</label>";
                            echo "<input type='date' class='form-control' id='tanggal_transaksi' name='tanggal_transaksi' value='" . $row["tanggal_transaksi"] . "'>";
                            echo "</div>";
                            echo "<div class='mb-3'>";
                            echo "<label for='no_telp' class='form-label'>Nama Pengeluaran</label>";
                            echo "<input type='text' class='form-control' id='nama_pengeluaran' name='nama_pengeluaran' value='" . $row["nama_pengeluaran"] . "'>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='jumlah' class='form-label'>Jumlah</label>";
                            echo "<input type='number' class='form-control' id='jumlah' name='jumlah' value='" . $row["jumlah"] . "'>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='keterangan' class='form-label'>Keterangan</label>";
                            echo "<input type='text' class='form-control' id='keterangan' name='keterangan' value='" . $row["keterangan"] . "'>";
                            echo "</div>";

                            echo "</div>";
                            echo "<div class='modal-footer'>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                            echo "<button type='submit' class='btn btn-primary'>Simpan Perubahan</button>";
                            echo "</div>";
                            echo "</form>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";


                            $nomor++;
                        }



                        ?>
                    </tbody>
                </table>
            </div>
                </div>

            </div>




        </div>

        <!-- Content Row -->


    </div>


    <script>
        // Mendapatkan nilai angka dari PHP dan menyimpannya dalam variabel JavaScript
        var angka = <?php echo $total; ?>;
        var pengeluaran = <?php echo $pengeluaran; ?>;
        var hariini = <?php echo $totalHariIni; ?>;

        // Menggunakan metode toLocaleString() untuk memformat angka
        var angkaFormatted = angka.toLocaleString();
        var angkaFormattedpengeluaran = pengeluaran.toLocaleString();
        var angkaFormattedhariini =  hariini.toLocaleString();

        // Menetapkan hasil format ke elemen span dengan id "angkaFormatted"
        document.getElementById("total").textContent = "Rp." + angkaFormatted;
        document.getElementById("pengeluaran").textContent = "Rp." + angkaFormattedpengeluaran;
        document.getElementById("hariini").textContent ="Rp."+ angkaFormattedhariini;

        console.log(angkaFormatted);
        console.log(angkaFormattedpengeluaran);
        console.log(angkaFormattedhariini);
    </script>