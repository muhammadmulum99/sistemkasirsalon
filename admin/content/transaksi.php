<?php
require_once dirname(dirname(__DIR__)) . '/config.php';

$sql = "SELECT * FROM tabel_harga order by nama_jasa asc";
$result = $con->query($sql);
$transaksi = "SELECT * FROM transaksi order by tanggal_transaksi desc";
$hasil = $con->query($transaksi);
$formattedNumbers = [];
$formattedNumbers2 = [];
$formattedNumbers3 = [];
?>
<div class="container-fluid">

    <!-- data karyawan -->
    <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModal">
        Cari Pelanggan
    </button>

    <form action='../admin/aksiadmin/transaksi/add.php' method='POST'>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Pelanggan</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" placeholder="Nama Pelanggan" readonly required>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal</label>
            <div class="col-sm-6">
                <?php
                date_default_timezone_set('Asia/Jakarta');
                $currentDate = date('Y-m-d'); ?>
                <input type="date" class="form-control" id="tanggal_masuk" placeholder="Tanggal Masuk" name="tanggal_masuk" value="<?php echo $currentDate; ?>" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Jenis Layanan</label>
            <div class="col-sm-6">
                <label for="exampleFormControlSelect1">Pilih Jenis layanan</label>
                <select class="form-control" id="layanan" name="layanan[]" onchange="getSelectedOption()" multiple required>
                    <option value="">Pilih Layanan dahulu</option>
                    <?php
                    foreach ($result as $data) {
                        // echo '<option value="' . $data['harga'] . '">' . $data['nama_jasa'] . '</option>';
                        echo '<option value="' . $data['nama_jasa'] . '" data-nama-jasa="' . $data['harga'] . '">' . $data['nama_jasa'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Tarif</label>
            <div class="col-sm-6">
                <input type="number" class="form-control" id="tarif" name="tarif" placeholder="Tarif" required>
                <input type="hidden" class="form-control" id="tarifmulti" name="tarifmulti[]" placeholder="Tarif" multiple required>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Dibayar</label>
            <div class="col-sm-6">
                <input type="number" class="form-control" id="dibayar" name="dibayar" placeholder="dibayar" onchange="getDibayar()" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Kembalian</label>
            <div class="col-sm-6">
                <input type="number" class="form-control" id="kembalian" name="kembalian" placeholder="Kembalian" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Nama Karyawan</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="nama_karyawan" required name="nama_karyawan" value="<?php echo $_SESSION['username'] ?>" placeholder="Nama Karyawan" readonly>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Tambahkan</button>
                <button onclick="location.reload(true)" class="btn btn-success">Clear</button>
            </div>

        </div>
    </form>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 text-center">Halaman Transaksi</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables transaksi</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Kode Transaksi</th>
                            <th>Nama Pelanggan</th>
                            <th>Tanggal</th>
                            <th>Jenis Pembayaran</th>
                            <th>Tarif</th>
                            <th>Dibayar</th>
                            <th>Kembalian</th>
                            <th>Nama Karyawan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $nomor = 1;

                        while ($row = $hasil->fetch_assoc()) {
                            $number = $row['kembalian'];
                            $number2 = $row['tarif'];
                            $number3 = $row['dibayar'];
                            // Format angka menggunakan metode toLocaleString()
                            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
                            $formattedNumber = number_format($number, 0, '.', ',');
                            $formattedNumber2 = number_format($number2, 0, '.', ',');
                            $formattedNumber3 = number_format($number2, 0, '.', ',');
                            // Menyimpan hasil formatted number ke dalam array
                            $formattedNumbers[] = $formattedNumber;
                            $formattedNumbers2[] = $formattedNumber2;
                            $formattedNumbers3[] = $formattedNumber3;
                            echo "<tr>";
                            echo "<td>" . $nomor . "</td>";
                            echo "<td>" . 'KD'.$row['id'].$row['tanggal_transaksi'] . "</td>";
                            echo "<td>" . $row["nama_pelanggan"] . "</td>";
                            echo "<td>" .date("d-m-Y", strtotime($row["tanggal_transaksi"])). "</td>";
                            echo "<td>" . $row["jenis_layanan"] . "</td>";
                            echo "<td>" . $formattedNumber2 . "</td>";
                            echo "<td>" . $formattedNumber3 . "</td>";
                            echo "<td>" . $formattedNumber . "</td>";
                            echo "<td>" . $row["nama_karyawan"] . "</td>";
                            echo "<td>";
                            echo "<div class='btn-group' role='group'>";
                            echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#editModal" . $row["id"] . "'>
                            <i class='fas fa-solid fa-eye'></i>
                            </button>";
                            echo " | ";
                            echo "<form action='aksiadmin/transaksi/nota.php' method='post'>
                            <input type='hidden' name='id' value='" . $row["id"] . "'>
                            <button type='submit' class='btn btn-danger'>
                            <i class='fas fa-regular fa-file-pdf'></i>
                            </button>
                            </form>";
                            echo " | ";
                            echo "<button type='button' class='btn btn-danger' onclick='confirmDelete(" . $row["id"] . ")'><i class='fas fa-trash'></i></button>";
                            echo "</div>"; // Close the btn-group div
                            echo "</td>";
                            echo "</tr>";
                            // Modal edit untuk setiap baris data
                            // Modal edit untuk setiap baris data
                            echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                            echo "<div class='modal-dialog'>";
                            echo "<div class='modal-content'>";
                            echo "<div class='modal-header'>";
                            echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                            echo "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>";
                            echo "</div>";
                            echo "<div class='modal-body'>";
                            echo "<form action='./aksiadmin/datakaryawan/edit.php' method='POST'>";
                            echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                            echo "<div class='mb-3'>";
                            echo "<label for='nama_karyawan' class='form-label'>Nama Karyawan</label>";
                            echo "<input type='text' class='form-control' id='nama_karyawan' name='nama_pelanggan' value='" . $row["nama_pelanggan"] . "' readonly>";
                            echo "</div>";
                            echo "<div class='mb-3'>";
                            echo "<label for='alamat' class='form-label'>Alamat</label>";
                            echo "<input type='text' class='form-control' id='alamat' name='alamat' value='" . $row["tanggal_transaksi"] . "'readonly>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='no_telp' class='form-label'>No Telp</label>";
                            echo "<input type='text' class='form-control' id='no_telp' name='no_telp' value='" . $row["jenis_layanan"] . "'readonly>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='tarif' class='form-label'>Tarif</label>";
                            echo "<input type='text' class='form-control' id='tarif' name='tarif' value='" . $row["tarif"] . "'readonly>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='dibayar' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='dibayar' name='dibayar' value='" . $row["dibayar"] . "'readonly>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='kembalian' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='kembalian' name='jabatan' value='" . $row["kembalian"] . "'readonly>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='nama_karyawan' class='form-label'>Jabatan</label>";
                            echo "<input type='text' class='form-control' id='nama_karyawan' name='nama_karyawan' value='" . $row["nama_karyawan"] . "'readonly>";
                            echo "</div>";

                            echo "</div>";
                            echo "<div class='modal-footer'>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                            echo "</div>";
                            echo "</form>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";
                            $nomor++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- modal add pelanggan -->
<div class="modal fade" id="modaladd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal Add</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action='./aksiadmin/datapelanggan/add.php' method='POST'>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Pelanggan </label>
                        <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" aria-describedby="emailHelp" placeholder="Enter Nama pelanggan">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">No Telepon</label>
                        <input type="text" class="form-control" id="no_telp" placeholder="No Telpon" name="no_telp">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal masuk</label>
                        <input type="date" class="form-control" id="tanggal_masuk" placeholder="Tanggal Masuk" name="tanggal_masuk">
                    </div>





                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>



                <div id="a"></div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal Add</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama karyawan </label>
                        <input type="text" class="form-control" id="search_pelanggan" name="search_pelanggan" onchange="searchPelanggan()" aria-describedby="emailHelp" placeholder="Enter Nama Jasa">

                    </div>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>


                <div id="searchResults"></div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    function confirmDelete(id) {
        Swal.fire({
            title: 'Konfirmasi Hapus Data',
            text: 'Apakah Anda yakin ingin menghapus data ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                // Redirect atau melakukan proses delete ke halaman delete.php dengan mengirimkan parameter ID
                window.location.href = './aksiadmin/transaksi/delete.php?id=' + id;
            }
        });
    }
    </script>



<script>
    function test() {
        alert('This is');
    }


    // function getSelectedOption() {
    //     var selectElement = document.getElementById("layanan");
    //     var selectedValue = selectElement.value;
    //     var selectedNamaJasa = selectElement.options[selectElement.selectedIndex].getAttribute("data-nama-jasa");
    //     console.log(selectedNamaJasa); // Output selected value to the browser console

    //     var nilaiTerpilihInput = document.getElementById("tarif");
    //     nilaiTerpilihInput.value = selectedNamaJasa; // Set the selected value to the input field
    //     document.getElementById("test").appendChild(selectElement);
    // }

    function getSelectedOption() {
    var selectElement = document.getElementById("layanan");
    var selectedOptions = Array.from(selectElement.selectedOptions);
    var selectedValues = selectedOptions.map(option => option.value);
    var selectedNamaJasaValues = selectedOptions.map(option => parseInt(option.getAttribute("data-nama-jasa")));

    console.log("Selected Values: ", selectedValues);
    console.log("Selected Nama Jasa Values: ", selectedNamaJasaValues);

    // Menghitung total tarif dari opsi yang dipilih
    var totalTarif = selectedNamaJasaValues.reduce((acc, currentValue) => acc + currentValue, 0);
    console.log("Total Tarif: ", totalTarif);

    // Menampilkan total tarif di dalam input field dengan ID "tarif"
    var tarifElement = document.getElementById("tarif");
    var tarifElementmulti = document.getElementById("tarifmulti");
    tarifElement.value = totalTarif;
    tarifElementmulti.value = totalTarifmulti;

    // Panggil fungsi getDibayar dengan nilai-nilai yang dipilih
    getDibayar(selectedValues, selectedNamaJasaValues);
}

    function getDibayar() {
        var dibayarElement = document.getElementById("dibayar");
        var tarifElement = document.getElementById("tarif");
        var kembalianElement = document.getElementById("kembalian");

        var dibayarValue = parseInt(dibayarElement.value);
        var tarifValue = parseInt(tarifElement.value);

        if (dibayarValue < tarifValue) {
            Swal.fire({
                icon: "error",
                title: "Error",
                text: "Nilai yang dibayar harus lebih tinggi dari tarif."
            }).then(function() {
                dibayarElement.value = ""; // Mengosongkan input "dibayar"
                kembalianElement.value = ""; // Mengosongkan input "kembalian"
            });
            return;
        }

        var hasil = dibayarValue - tarifValue;
        kembalianElement.value = hasil; // Set the calculated value to the "kembalian" input field
        console.log(kembalianElement.value); // Output
    }

//     function getDibayar(selectedValues, selectedNamaJasaValues) {
//     // Menghitung total tarif dari opsi yang dipilih
//     var totalTarif = 0;
//     for (var i = 0; i < selectedValues.length; i++) {
//         var tarifValue = parseInt(selectedNamaJasaValues[i]);
//         totalTarif += tarifValue;
//     }

//     var dibayarElement = document.getElementById("dibayar");
//     var kembalianElement = document.getElementById("kembalian");

//     var dibayarValue = parseInt(dibayarElement.value);

//     if (dibayarValue < totalTarif) {
//         Swal.fire({
//             icon: "error",
//             title: "Error",
//             text: "Nilai yang dibayar harus lebih tinggi dari total tarif."
//         }).then(function() {
//             dibayarElement.value = ""; // Mengosongkan input "dibayar"
//             kembalianElement.value = ""; // Mengosongkan input "kembalian"
//         });
//         return;
//     }

//     var hasil = dibayarValue - totalTarif;
//     kembalianElement.value = hasil; // Set the calculated value to the "kembalian" input field
//     console.log(kembalianElement.value); // Output
// }


    function searchPelanggan() {
        var searchValue = document.getElementById('search_pelanggan').value;
        var apiUrl = '../../skripsisemarang/transaksi/aksi/get_pelanggan.php?search=' + encodeURIComponent(searchValue);

        // Permintaan AJAX menggunakan jQuery
        $.ajax({
            url: apiUrl,
            method: 'GET',
            dataType: 'json',
            success: function(response) {
                // Menyimpan hasil pencarian dalam localStorage
                localStorage.setItem('searchResults', JSON.stringify(response));
                console.info(response)

                // Tampilkan hasil pencarian atau lakukan aksi lainnya
                displaySearchResults(response);
            },
            error: function() {
                console.log('Terjadi kesalahan dalam melakukan pencarian.');
                console.info(response)
            }
        });
    }

    function displaySearchResults(results) {
        var tableContainer = document.getElementById('searchResults');
        tableContainer.innerHTML = '';

        if (results && results.length > 0) {
            // Membuat elemen tabel
            var table = document.createElement('table');
            table.classList.add('table');

            // Membuat header tabel
            var tableHeader = document.createElement('thead');
            var headerRow = document.createElement('tr');
            var headerNama = document.createElement('th');
            headerNama.textContent = 'Nama';
            headerRow.appendChild(headerNama);
            tableHeader.appendChild(headerRow);
            table.appendChild(tableHeader);

            // Membuat isi tabel
            var tableBody = document.createElement('tbody');
            results.forEach(function(result) {
                var row = document.createElement('tr');
                var namaCell = document.createElement('td');
                namaCell.textContent = result.nama_pelanggan;
                row.appendChild(namaCell);

                // Membuat tombol untuk setiap hasil pencarian
                var buttonCell = document.createElement('td');
                var button = document.createElement('button');
                button.classList.add('btn', 'btn-primary');
                button.textContent = 'Pilih';
                button.addEventListener('click', function() {
                    // Lakukan aksi yang diinginkan saat tombol ditekan
                    var namaPelangganIsi = document.getElementById('nama_pelanggan');
                    namaPelangganIsi.value = result.nama_pelanggan;
                    console.log('Anda memilih pelanggan: ' + result.nama_pelanggan);

                    var modal = document.getElementById('exampleModal');
                    $(modal).modal('hide');
                });
                buttonCell.appendChild(button);
                row.appendChild(buttonCell);

                tableBody.appendChild(row);
            });
            table.appendChild(tableBody);

            // Menambahkan tabel ke kontainer
            tableContainer.appendChild(table);
        } else {
            var message = document.createElement('p');
            message.textContent = 'Tidak ada hasil ditemukan.';
            tableContainer.appendChild(message);

        }
    }
</script>
