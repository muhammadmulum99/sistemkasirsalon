<?php
require_once dirname(dirname(__DIR__)) . '/config.php';


$sql = "SELECT * FROM tabel_harga";
$result = $con->query($sql);

$transaksi = "SELECT * FROM tabel_pengeluaran";
$hasil = $con->query($transaksi);

$formattedNumbers = [];
?>


<div class="container-fluid">

    <!-- data karyawan -->

    <form action='./aksiadmin/pengeluaran/add.php' method='POST'>
     
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Tanggal Transaksi</label>
            <div class="col-sm-3">
                <?php
                date_default_timezone_set('Asia/Jakarta');
                $currentDate = date('Y-m-d'); ?>
                <input type="date" class="form-control" id="tanggal_transaksi" placeholder="Tanggal Masuk" name="tanggal_transaksi" value="<?php echo $currentDate; ?>" required>
            </div>
            
        </div>

      

        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Nama Pengeluaran</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="nama_pengeluaran" name="nama_pengeluaran" placeholder="Rp. XXX.XXX" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Total Pengeluaran</label>
            <div class="col-sm-6">
                <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Rp. XXX.XXX"  required>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Keterangan</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterabfab" required>
            </div>
        </div>



        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Tambahkan</button>
                <button id="reloadButton" type="" class="btn btn-success">Clear</button>
            </div>

        </div>
    </form>

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800 text-center">Halaman Pengeluaran</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Pengeluaran</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Tanggal Transaksi</th>
                            <th>Nama Pengeluaran</th>
                            <th>Jumlah Pengeluaran</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                  
                    <tbody>
                        <?php
                        $nomor = 1;

                        while ($row = $hasil->fetch_assoc()) {

                            $number = $row['jumlah'];
        
                            // Format angka menggunakan metode toLocaleString()
                            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
                            $formattedNumber = number_format($number, 0, '.', ',');
                            
                            // Menyimpan hasil formatted number ke dalam array
                            $formattedNumbers[] = $formattedNumber;
                            echo "<tr>";
                            echo "<td>" . $nomor . "</td>";
                            echo "<td>" . $row["tanggal_transaksi"] . "</td>";
                            echo "<td>" . $row["nama_pengeluaran"] . "</td>";
                            echo "<td>" . $formattedNumber . "</td>";
                            echo "<td>" . $row["keterangan"] . "</td>";
                            echo "<td>";
                            echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#editModal" . $row["id"] . "'>
                            <i class='fas fa-edit'></i>
                          </button>"; // Tombol delete dengan link ke halaman delete.php
                            echo " | ";
                            echo "<button type='button' class='btn btn-danger' onclick='confirmDelete(" . $row["id"] . ")'><i class='fas fa-trash'></i></button>"; // Tombol delete dengan SweetAlert konfirmasi

                            echo "</td>";
                            echo "</tr>";


                            // Modal edit untuk setiap baris data
                            // Modal edit untuk setiap baris data
                            echo "<div class='modal fade' id='editModal" . $row["id"] . "' tabindex='-1' aria-labelledby='editModalLabel" . $row["id"] . "' aria-hidden='true'>";
                            echo "<div class='modal-dialog'>";
                            echo "<div class='modal-content'>";
                            echo "<div class='modal-header'>";
                            echo "<h5 class='modal-title' id='editModalLabel" . $row["id"] . "'>Edit Data ID " . $row["id"] . "</h5>";
                            echo "<button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>";
                            echo "</div>";
                            echo "<div class='modal-body'>";
                            echo "<form action='./aksiadmin/pengeluaran/edit.php' method='POST'>";
                            echo "<input type='hidden' name='id' value='" . $row["id"] . "'>";
                            echo "<div class='mb-3'>";
                            echo "<label for='tanggal_transaksi' class='form-label'>Tanggal Transaksi</label>";
                            echo "<input type='date' class='form-control' id='tanggal_transaksi' name='tanggal_transaksi' value='" . $row["tanggal_transaksi"] . "'>";
                            echo "</div>";
                            echo "<div class='mb-3'>";
                            echo "<label for='no_telp' class='form-label'>Nama Pengeluaran</label>";
                            echo "<input type='text' class='form-control' id='nama_pengeluaran' name='nama_pengeluaran' value='" . $row["nama_pengeluaran"] . "'>";
                            echo "</div>";

                            echo "<div class='mb-3'>";
                            echo "<label for='jumlah' class='form-label'>Jumlah</label>";
                            echo "<input type='number' class='form-control' id='jumlah' name='jumlah' value='" . $row["jumlah"] . "'>";
                            echo "</div>";


                            echo "<div class='mb-3'>";
                            echo "<label for='keterangan' class='form-label'>Keterangan</label>";
                            echo "<input type='text' class='form-control' id='keterangan' name='keterangan' value='" . $row["keterangan"] . "'>";
                            echo "</div>";

                            echo "</div>";
                            echo "<div class='modal-footer'>";
                            echo "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Tutup</button>";
                            echo "<button type='submit' class='btn btn-primary'>Simpan Perubahan</button>";
                            echo "</div>";
                            echo "</form>";
                            echo "</div>";
                            echo "</div>";
                            echo "</div>";


                            $nomor++;
                        }



                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>





<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>


 // Menggunakan JavaScript untuk menambahkan fungsi reload
 document.getElementById("reloadButton").addEventListener("click", function () {
            location.reload();
        });


    function confirmDelete(id) {
        Swal.fire({
            title: 'Konfirmasi Hapus Data',
            text: 'Apakah Anda yakin ingin menghapus data ini?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                // Redirect atau melakukan proses delete ke halaman delete.php dengan mengirimkan parameter ID
                window.location.href = './aksiadmin/pengeluaran/delete.php?id=' + id;
            }
        });
    }
</script>