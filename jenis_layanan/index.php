<?php
include '../config.php';

// mengaktifkan session
session_start();

// cek apakah user telah login, jika belum login maka di alihkan ke halaman login
if ($_SESSION['status'] != "login") {
	header("location:../index.php");
}

// menampilkan pesan selamat datang
// echo "Hai, selamat datang " . $_SESSION['username'];

?>


<?php include("../template/header.php"); ?>


<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<?php include "../template/sidebar.php" ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<?php include '../template/topbar.php'; ?>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<?php include "./content/dashboardlayanan.php"; ?>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->

			<?php include("../template/footer.php"); ?>