<?php
require_once dirname(dirname(__DIR__)) . '/config.php';


$sql = "SELECT * FROM tabel_harga order by nama_jasa asc";
$result = $con->query($sql);
$formattedNumbers = [];


?>


<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables Harga Layanan</h1>
 


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data layanan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Layanan Jasa</th>
                            <th>Harga</th>
                           
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Jenis Layanan Jasa</th>
                            <th>Harga</th>
                           
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $no = 1;
                        while ($row = $result->fetch_assoc()) {
                            $number = $row['harga'];
        
                            // Format angka menggunakan metode toLocaleString()
                            // Anda dapat menyesuaikan locale dan opsi sesuai kebutuhan
                            $formattedNumber = number_format($number, 0, '.', ',');
                            
                            // Menyimpan hasil formatted number ke dalam array
                            $formattedNumbers[] = $formattedNumber;
                            echo "<tr>";
                            echo "<td>" . $no. "</td>";
                            echo "<td>" . $row["nama_jasa"] . "</td>";
                            echo "<td>" . $formattedNumber . "</td>";
                            echo "</tr>";
                            $no++;

                
                        }



                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>