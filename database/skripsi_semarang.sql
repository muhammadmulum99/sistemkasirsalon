-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2023 at 11:24 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_semarang`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_harga`
--

CREATE TABLE `tabel_harga` (
  `id` int(11) NOT NULL,
  `nama_jasa` varchar(150) NOT NULL,
  `harga` int(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tabel_harga`
--

INSERT INTO `tabel_harga` (`id`, `nama_jasa`, `harga`) VALUES
(1, 'Gunting rambut', 25000),
(2, 'Creambath', 50000),
(3, 'Creambath plus catok', 75000),
(4, 'Potong poni ', 5000),
(5, 'Smothing pendek', 150000),
(6, 'Smothing panjang', 300000),
(7, 'Rebonding pendek', 200000),
(8, 'Rebonding panjang ', 400000),
(9, 'Lulur', 200000),
(10, 'Facial komplit', 150000),
(11, 'Facial biasa', 100000),
(12, 'Hair mask', 75000),
(13, 'Make up sanggul ', 70000),
(14, 'Alis', 5000),
(16, 'Nail art kaki ', 100000),
(17, 'Nail art tangan ', 140000),
(18, 'Highlight ', 0),
(19, 'Coloring rambut ', 0),
(22, 'ss', 400000);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_karyawan`
--

CREATE TABLE `tabel_karyawan` (
  `id` int(11) NOT NULL,
  `nama_karyawan` varchar(250) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(100) NOT NULL,
  `jabatan` varchar(150) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tabel_karyawan`
--

INSERT INTO `tabel_karyawan` (`id`, `nama_karyawan`, `alamat`, `no_telp`, `jabatan`, `username`, `password`) VALUES
(1, 'tes karyawan', 'alamat', '2323', 'tes', 'dasda', '2e0aca891f2a8aedf265edf533a6d9a8'),
(5, 'czxczxc', 'xzczxc', 'wewe', 'dasd', 'dasda', '202cb962ac59075b964b07152d234b70'),
(6, 'karyawan1', 'tes', '3231', 'karyawan', 'karyawan1', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pelanggan`
--

CREATE TABLE `tabel_pelanggan` (
  `id` int(11) NOT NULL,
  `nama_pelanggan` varchar(250) NOT NULL,
  `no_telp` varchar(150) NOT NULL,
  `tanggal_masuk` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tabel_pelanggan`
--

INSERT INTO `tabel_pelanggan` (`id`, `nama_pelanggan`, `no_telp`, `tanggal_masuk`) VALUES
(1, 'test pelanggan', '232', '2023-07-20'),
(3, 'test pelanggan2', '22', '2023-07-12'),
(6, 'test pelanggan', '23', '2023-08-08');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pengeluaran`
--

CREATE TABLE `tabel_pengeluaran` (
  `id` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `nama_pengeluaran` varchar(150) NOT NULL,
  `jumlah` int(150) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tabel_pengeluaran`
--

INSERT INTO `tabel_pengeluaran` (`id`, `tanggal_transaksi`, `nama_pengeluaran`, `jumlah`, `keterangan`) VALUES
(1, '2023-07-03', 'dasda', 110000, 'dasdasd'),
(3, '2023-07-02', 'asdasd', 233, 'dasdasd'),
(4, '2023-08-06', 'asasa', 200000, 'fsdfsd'),
(5, '2023-09-28', 'fsfsdf', 3434, 'hgfh');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `nama_pelanggan` varchar(250) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `jenis_layanan` varchar(100) NOT NULL,
  `tarif` varchar(150) NOT NULL,
  `dibayar` varchar(150) NOT NULL,
  `kembalian` varchar(150) NOT NULL,
  `nama_karyawan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `nama_pelanggan`, `tanggal_transaksi`, `jenis_layanan`, `tarif`, `dibayar`, `kembalian`, `nama_karyawan`) VALUES
(6, 'test pelanggan', '2023-07-28', 'Potong poni ', '5000', '10000', '5000', 'admin'),
(7, 'test pelanggan', '2023-07-28', 'Alis', '5000', '6000', '1000', 'admin'),
(8, 'test pelanggan', '2023-07-28', 'Alis', '5000', '6000', '1000', 'admin'),
(9, 'test pelanggan', '2023-07-28', 'Alis', '5000', '6000', '1000', 'admin'),
(10, 'test pelanggan', '2023-07-28', 'Rebonding pendek', '200000', '200000', '0', 'karyawan1'),
(12, 'test pelanggan', '2023-08-02', 'Lulur', '200000', '200000', '0', 'admin'),
(13, 'test pelanggan2', '2023-08-06', 'Alis', '5000', '200000', '195000', 'admin'),
(14, 'test pelanggan', '2023-08-06', 'Alis', '5.000', '200.000', '195000', 'karyawan1'),
(18, 'test pelanggan', '2023-08-29', 'Creambath', '50000', '200000', '150000', 'admin'),
(19, 'test pelanggan', '2023-09-11', 'Cuci catok', '25000', '200000', '175000', 'admin'),
(21, 'test pelanggan', '2023-09-28', 'Alis', '5000', '200000', '195000', 'admin'),
(22, 'test pelanggan', '2023-09-28', 'Hair mask', '75000', '200000', '125000', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`) VALUES
(1, 'test', 'test', '098f6bcd4621d373cade4e832627b4f6'),
(3, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_harga`
--
ALTER TABLE `tabel_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_karyawan`
--
ALTER TABLE `tabel_karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_pelanggan`
--
ALTER TABLE `tabel_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_pengeluaran`
--
ALTER TABLE `tabel_pengeluaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_harga`
--
ALTER TABLE `tabel_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tabel_karyawan`
--
ALTER TABLE `tabel_karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tabel_pelanggan`
--
ALTER TABLE `tabel_pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tabel_pengeluaran`
--
ALTER TABLE `tabel_pengeluaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
