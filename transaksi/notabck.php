<?php
// ... kode untuk koneksi ke database ...
require_once dirname(__DIR__) . '/config.php';

// Import FPDF library
require_once 'fpdf186/fpdf.php';

if (isset($_POST['id'])) {
    // ... Kode pengolahan data seperti yang telah diberikan sebelumnya ...
    $idtransaksi = $_POST['id'];

    $query = "SELECT * FROM transaksi WHERE id = '$idtransaksi'";
    $result = mysqli_query($con, $query);
    $transaksiarr = array();
    $transaksi = mysqli_fetch_assoc($result);

  

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }

    $pdf = new FPDF(); // Buat objek PDF
    function HeaderNota()
    {
        // Judul nota
        $GLOBALS['pdf']->SetFont('Arial', 'B', 12);
        $GLOBALS['pdf']->Cell(0, 5, 'NOTA TRANSAKSI SALON', 0, 1, 'C');
        $GLOBALS['pdf']->Cell(0, 5, 'Viny Salon', 0, 1, 'C');
        $GLOBALS['pdf']->Cell(0, 5, 'Lahewa Nias Utara', 0, 1, 'C');
        $GLOBALS['pdf']->SetFont('Arial', '', 10); // Ganti font dan ukuran teks
        $GLOBALS['pdf']->Cell(0, 10, 'Jl. Beringin 2, (belakang kantor POS) Kec. Lahewa, Kab. Nias Utara Prov. Sumatera Utara', 0, 1, 'C');
        //$GLOBALS['pdf']->Image('http://localhost/skripsisemarang/admin/aksiadmin/datalaporan/logos.png', 40, 10, $logoWidth, $logoHeight);
        $GLOBALS['pdf']->Ln(5);

        // Garis atas header
        $GLOBALS['pdf']->Cell(0, 0, '', 'T', 1, 'C');
        $GLOBALS['pdf']->Ln(5);
    }

    // Fungsi untuk menggambar footer
    function FooterNota()
    {
        $GLOBALS['pdf']->SetY(-1);
        $GLOBALS['pdf']->SetFont('Arial', 'I', 8);
        $GLOBALS['pdf']->Cell(0, 10, 'Viny Salon', 0, 0, 'C');
    }

    // Fungsi untuk menggambar konten transaksi
    function ContentNota($transaksi)
    {
        // Teks konten
        $GLOBALS['pdf']->SetFont('Arial', '', 10);
        $GLOBALS['pdf']->Cell(50, 6, 'Tanggal: ' . $transaksi['tanggal_transaksi'], 0, 1);
        $GLOBALS['pdf']->Cell(50, 6, 'Nama Pelanggan: ' . $transaksi['nama_pelanggan'], 0, 1);
        $GLOBALS['pdf']->Cell(50, 6, 'Email: ' . 'email@mail.com', 0, 1);
        $GLOBALS['pdf']->Cell(50, 6, 'No Telepon: ' . '08080808', 0, 1);
        $GLOBALS['pdf']->Ln(5);

        // Item transaksi
        $GLOBALS['pdf']->SetFont('Arial', 'B', 12);
        $GLOBALS['pdf']->Cell(30, 10, 'Kode Transaksi', 0, 0, 'C');
       
        $GLOBALS['pdf']->Cell(40, 10, 'Layanan', 0, 0, 'C');
        $GLOBALS['pdf']->Cell(40, 10, 'Tarif', 0, 0, 'C');
        $GLOBALS['pdf']->Cell(40, 10, 'Dibayar', 0, 0, 'C');
        $GLOBALS['pdf']->Cell(40, 10, 'Kembalian', 0, 1, 'C');

        $GLOBALS['pdf']->SetFont('Arial', '', 12);
        $no = 1;
        $GLOBALS['pdf']->Cell(30, 10, 'KD'.$transaksi['id'].$transaksi['tanggal_transaksi'], 0, 0, 'C');
        $GLOBALS['pdf']->Cell(40, 10, $transaksi['jenis_layanan'], 0, 0,'C');
        $GLOBALS['pdf']->Cell(40, 10, 'Rp ' . number_format(floatval($transaksi['tarif'])), 0, 0, 'C');
        $GLOBALS['pdf']->Cell(40, 10, 'Rp ' . number_format(floatval($transaksi['dibayar'])), 0, 0, 'C');
        $GLOBALS['pdf']->Cell(40, 10, 'Rp ' . number_format(floatval($transaksi['kembalian'])), 0, 1, 'C');

        
        // foreach ($transaksi['layanan'] as $layanan) {
        //     $GLOBALS['pdf']->Cell(30, 10, $no, 0, 0, 'C');
        //     $GLOBALS['pdf']->Cell(80, 10, $layanan['nama'], 0, 0);
        //     $GLOBALS['pdf']->Cell(40, 10, 'Rp ' . number_format($layanan['harga']), 0, 1, 'R');
        //     $no++;
        // }

        // Total harga
        $GLOBALS['pdf']->SetFont('Arial', 'B', 12);
        $GLOBALS['pdf']->Cell(110, 10, 'Total Harga', 0, 0, 'R');
        $GLOBALS['pdf']->Cell(40, 10, 'Rp ' . number_format(floatval($transaksi['tarif'])), 0, 1, 'R');

        // Tanda tangan
        $ttd_y = $GLOBALS['pdf']->GetY() + 20;
        // $GLOBALS['pdf']->SetFont('Arial', '', 12);
        // $GLOBALS['pdf']->SetXY(140, $ttd_y);
        // $GLOBALS['pdf']->Cell(60, 10, 'Tanda tangan', 0, 1, 'R');

        // Alamat dan tanggal
        $alamat_y = $ttd_y - 15;
        $GLOBALS['pdf']->SetFont('Arial', '', 12);
        $GLOBALS['pdf']->SetXY(140, $alamat_y);
        $GLOBALS['pdf']->Cell(60, 10, 'Lahewa : ' . $transaksi['tanggal_transaksi'], 0, 1, 'R');
        $GLOBALS['pdf']->SetXY(140, $alamat_y + 5);
        // $GLOBALS['pdf']->Cell(60, 10, $transaksi['tanggal_transaksi'], 0, 1, 'R');
        $GLOBALS['pdf']->Ln(45);


        $teks_y = $ttd_y + 15;
        $GLOBALS['pdf']->SetXY(75, $teks_y);
        $GLOBALS['pdf']->SetFont('Arial', '', 10);
        $GLOBALS['pdf']->Cell(0, 10, 'Tanda tangan', 0, 1, 'R');
    }

    // Contoh data transaksi salon
    // $data_transaksi = array(
    //     'tanggal' => '2023-08-06',
    //     'nama_pelanggan' => 'John Doe',
    //     'alamat_pelanggan' => 'Jl. Contoh No. 123',
    //     'layanan' => array(
    //         array('nama' => 'Potong Rambut', 'harga' => 50000),
    //         array('nama' => 'Creambath', 'harga' => 75000),
    //         array('nama' => 'Pijat Refleksi', 'harga' => 90000),
    //     ),
    //     'total_harga' => 215000,
    // );

    // Instansiasi PDF dan buat halaman baru
    $pdf = new FPDF();
    $pdf->AddPage();

    // Panggil fungsi untuk menggambar header, konten, dan footer
    $GLOBALS['pdf'] = $pdf;
    HeaderNota();
    ContentNota($transaksi);
    FooterNota();

    // Output PDF
    $pdf->Output();

    mysqli_close($con);
} else {
    echo "<p>Silakan isi tanggal awal dan tanggal akhir.</p>";
}
