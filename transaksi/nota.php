<?php
// ... kode untuk koneksi ke database ...
require_once dirname(__DIR__) . '/config.php';

// Import FPDF library
require_once 'fpdf186/fpdf.php';
if (isset($_POST['id'])) {

  

    $pdo = new PDO("mysql:host=localhost;dbname=skripsi_semarang", "root", "");

    $idtransaksi = $_POST['id'];
    $query = "SELECT * FROM transaksi WHERE id = '$idtransaksi'";
    $result = mysqli_query($con, $query);
    $transaksiarr = array();
    $transaksi = mysqli_fetch_assoc($result);
    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $transaksi[] = $row;
        }
    }


    $searchTerms = explode(",", $transaksi['jenis_layanan']);

    // Query SQL awal tanpa klausa WHERE
    $sql = "SELECT * FROM tabel_harga";

    // Array untuk menyimpan klausa WHERE
    $whereClauses = [];

    // Array untuk menyimpan nilai parameter pencarian
    $searchParams = [];

    // Membuat klausa WHERE dan parameter dinamis berdasarkan nilai pencarian
    foreach ($searchTerms as $key => $term) {
        $param = ":search" . $key;
        $whereClauses[] = "nama_jasa LIKE " . $param;
        $searchParams[$param] = '%' . trim($term) . '%';
    }

    // Menggabungkan klausa WHERE
    if (!empty($whereClauses)) {
        // $sql .= " WHERE " . implode(" OR ", $whereClauses);
        // $sql .= " WHERE nama_jasa IN (" . implode(",", $whereClauses) . ")";
        $sql .= " WHERE nama_jasa IN ('" . implode("','", $searchTerms) . "')";
    }

    // Menyiapkan statement SQL
    $stmt = $pdo->prepare($sql);

    // Mengikat nilai parameter dengan nilai-nilai pencarian
    foreach ($searchParams as $param => $value) {
        $stmt->bindParam($param, $value);
    }

    // Menjalankan kueri
    $stmt->execute();

    // Mendapatkan hasil kueri
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);


    $pdf = new FPDF(); // Buat objek PDF
    function HeaderNota()
    {
        // Judul nota
        $GLOBALS['pdf']->SetFont('Arial', 'B', 12);
        $GLOBALS['pdf']->Cell(0, 5, 'NOTA TRANSAKSI SALON', 0, 1, 'C');
        $GLOBALS['pdf']->Cell(0, 5, 'Viny Salon', 0, 1, 'C');
        $GLOBALS['pdf']->Cell(0, 5, 'Lahewa Nias Utara', 0, 1, 'C');
        $GLOBALS['pdf']->SetFont('Arial', '', 10); // Ganti font dan ukuran teks
        $GLOBALS['pdf']->Cell(0, 10, 'Jl. Beringin 2, (belakang kantor POS) Kec. Lahewa, Kab. Nias Utara Prov. Sumatera Utara', 0, 1, 'C');
        //$GLOBALS['pdf']->Image('http://localhost/skripsisemarang/admin/aksiadmin/datalaporan/logos.png', 40, 10, $logoWidth, $logoHeight);
        $GLOBALS['pdf']->Ln(5);

        // Garis atas header
        $GLOBALS['pdf']->Cell(0, 0, '', 'T', 1, 'C');
        $GLOBALS['pdf']->Ln(5);
    }

    // Fungsi untuk menggambar footer
    function FooterNota()
    {
        $GLOBALS['pdf']->SetY(-1);
        $GLOBALS['pdf']->SetFont('Arial', 'I', 8);
        $GLOBALS['pdf']->Cell(0, 10, 'Viny Salon', 0, 0, 'C');
    }

    // Fungsi untuk menggambar konten transaksi
    function ContentNota($transaksi,$results)
    {
        if (isset($_SESSION['nama'])) {
            $namaPengguna = $_SESSION['nama'];
        } else {
            $namaPengguna = "Nama Pengguna Tidak Tersedia"; 
        }
        
        // Teks konten
        $GLOBALS['pdf']->SetFont('Arial', '', 10,);
        $GLOBALS['pdf']->Cell(0, 6, 'Tanggal: ' . $transaksi['tanggal_transaksi'], 0, 1,'C');
        $GLOBALS['pdf']->Cell(0, 6, 'Nama Pelanggan: ' . $transaksi['nama_pelanggan'], 0, 1,'C');
        $GLOBALS['pdf']->Cell(0, 6, 'KD Jasa : ' . 'KD'.$transaksi['id'].$transaksi['tanggal_transaksi'], 0, 1,'C');
        $GLOBALS['pdf']->Ln(5);

        $GLOBALS['pdf']->Cell(0, 0, '', 'T', 1, 'C');
        // Item transaksi
        $GLOBALS['pdf']->SetFont('Arial', 'B', 12);
      

        $GLOBALS['pdf']->SetFont('Arial', '', 12);
     
        foreach ($results as $result) {
            $namaJasa = $result['nama_jasa'];
            $hargaJasa = $result['harga'];
            // Operasi yang perlu Anda lakukan
            // Misalnya:
            $GLOBALS['pdf']->Cell(90, 10, $namaJasa, 0, 0, 'C');
            $GLOBALS['pdf']->Cell(0, 10, 'Rp ' . $hargaJasa, 0, 1, 'C');
        }



        // $GLOBALS['pdf']->Cell(90, 10, $transaksi['jenis_layanan'], 0, 0,'C');
        // $GLOBALS['pdf']->Cell(0, 10, 'Rp ' . number_format(floatval($transaksi['tarif'])), 0, 0, 'C');
        $GLOBALS['pdf']->Ln(15);
        $GLOBALS['pdf']->Cell(0, 0, '', 'T', 1, 'C');
      
        // Total harga
        $GLOBALS['pdf']->SetFont('Arial', '', 12);
        $GLOBALS['pdf']->Cell(80, 10, 'Total Harga', 0, 0, 'C');
        $GLOBALS['pdf']->Cell(0, 10, 'Rp ' . number_format(floatval($transaksi['tarif'])), 0, 1, 'C');

        $GLOBALS['pdf']->Cell(80, 10, 'Dibayar', 0, 0, 'C');
        $GLOBALS['pdf']->Cell(0, 10, 'Rp ' . number_format(floatval($transaksi['dibayar'])), 0, 0, 'C');
        $GLOBALS['pdf']->Ln(10);
        $GLOBALS['pdf']->Cell(80, 10, 'Kembalian', 0, 0, 'C');
        $GLOBALS['pdf']->Cell(0, 10, 'Rp ' . number_format(floatval($transaksi['kembalian'])), 0, 1, 'C');
        $GLOBALS['pdf']->Cell(0, 0, '', 'T', 1, 'C');
        $GLOBALS['pdf']->Ln(5);
     

        // $teks_y = $ttd_y + 15;
        // $GLOBALS['pdf']->SetXY(0, $teks_y);
        $GLOBALS['pdf']->SetFont('Arial', 'B', 10);
        $GLOBALS['pdf']->Cell(0, 10,'Terima Kasih', 0, 1, 'C');
        $GLOBALS['pdf']->Ln(5);
        $GLOBALS['pdf']->SetFont('Arial', '', 10);
        $GLOBALS['pdf']->Cell(0, 10,'Jangan lupa kembali', 0, 1, 'C');
        $GLOBALS['pdf']->Cell(0, 0,'Di Vony Salon', 0, 1, 'C');
    }

  

    // Instansiasi PDF dan buat halaman baru
    $pdf = new FPDF();
    $pdf->AddPage();

    // Panggil fungsi untuk menggambar header, konten, dan footer
    $GLOBALS['pdf'] = $pdf;
    HeaderNota();
    ContentNota($transaksi,$results);
    // FooterNota();

    // Output PDF
    $pdf->Output();

    mysqli_close($con);
} else {
    echo "<p>Silakan isi tanggal awal dan tanggal akhir.</p>";
}
