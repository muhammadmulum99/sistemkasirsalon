<?php
// Lakukan koneksi ke database dan dapatkan objek koneksi ($connection)
require_once dirname(dirname(__DIR__)) . '/config.php';

// Cek apakah parameter pencarian ada
if (isset($_GET['search'])) {
    $search = $_GET['search'];

    // Lakukan query SQL untuk mencari pelanggan berdasarkan kata kunci
    $query = "SELECT * FROM tabel_pelanggan WHERE nama_pelanggan LIKE '%$search%'";
    $result = mysqli_query($con, $query);

    // Cek apakah query berhasil dijalankan
    if ($result) {
        $data = array();

        while ($row = mysqli_fetch_assoc($result)) {
            $data[] = $row;
        }

        // Mengirim data pelanggan dalam format JSON
        echo json_encode($data);
    } else {
        // Jika query gagal dijalankan
        echo json_encode(array('error' => 'Terjadi kesalahan dalam query.'));
    }
} else {
    // Jika parameter pencarian tidak ada
    echo json_encode(array('error' => 'Parameter pencarian tidak ditemukan.'));
}

// Tutup koneksi ke database
mysqli_close($con);
?>
